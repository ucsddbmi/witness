#!/bin/bash
# Purpose= Backup log fils of WITNESS project
#Created on 2-15-2015
#Author  Lichang Wang 

LOGS_PATH="/var/log/witness"
LOGS_BACKUP_PATH="logBackup"

cd $LOGS_PATH

log_files=$(find . -regex ".*\.csv$")
#log_files=$(ls $LOGS_PATH | grep ".*\.csv$")

file_num=${#log_files}

if [ -z "$log_files" ]
then
  echo "No log file found !"
  exit 0
fi

dest_folder="$LOGS_BACKUP_PATH"/`date +%Y`/`date +%m`

if ! [ -d $dest_folder ]
then
mkdir -p $dest_folder
fi

dest_file="$dest_folder"/logbackup_`date +%Y-%m-%d `.tar


log_files=${log_files//\.\//}

tar -czvf $dest_file  $log_files

rm $log_files

