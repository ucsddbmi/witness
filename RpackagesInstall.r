#!/usr/bin/Rscript
##install shiny 0.10.1 
##check and install shiny packages
cat("***************************************\n")
cat("install R packages")

shiny_reposit<-"http://cran.r-project.org/src/contrib/Archive/shiny/shiny_0.10.1.tar.gz"

shiny_reposit2<-"witness2.ucsd-dbmi.org/Sources/shiny_0.10.1.tar.gz"
reposit<-"http://cran.rstudio.com/"
R_libpath<-"/usr/local/lib/R/site-library"

required_packages<-c("shiny","stringr","gdata","rJava","mailR","RJSONIO","knitr","boot","gbm","pROC","ROCR","Daim","MKmisc","roxygen2","devtools","survival","psych","car","plyr","reshape2","vegan","permute","lattice","lubridate","slam","shinyAce","wordcloud","AlgDesign","Cairo")



.libPaths(R_libpath)
chooseCRANmirror(ind=81)

for(i in 2:length(required_packages)){
    if(!require(required_packages[i],character.only=T)){
      try(install.packages(required_packages[i],R_libpath,dependencies=T,repos=reposit))
    } 
  }

library(devtools)
install_url(shiny_reposit)

if(!require(required_packages[1],character.only=T)){
  install_url(shiny_reposit2)
}else if(packageVersion(required_packages[1])!="0.10.1"){
  remove.packages("shiny",R_libpath)
  install_url(shiny_reposit2)
}

