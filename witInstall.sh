#!/bin/bash

WIT_SUPP="witness2.ucsd-dbmi.org/Sources/witnessArc.tar"
WIT_LOCAL="./witnessArc.tar"
WIT_INSTALL_PATH="."
WIT_TEMP_DIR="/tmp/witInstall"
R_LIBRARY="/usr/local/lib/R/site-library"
LOG_PATH="/var/log/witness"
SHINY_SERVER="https://s3.amazonaws.com/rstudio-shiny-server-pro-build/ubuntu-12.04/x86_64/shiny-server-commercial-1.3.0.540-amd64.deb"
SSL_PKEY="/etc/shiny-server/witness_pkey.pem"
SSL_CERT="/etc/shiny-server/witness_cert.pem"
NGINX_CONF="/etc/nginx/sites-available/default"



wget -q --tries=20 --timeout=10 --spider "https://www.google.com"

if [  $? -ne 0 ]
   then
   echo "Internet is not available!"
   exit 1
fi

#############################################
# get command line parameters
#############################################
options=$@
arguments=($options)
index=0
##while getopts "winIn:winLc:winWb:rserverWb:ldapDm:ldapPw:" opt;   ##getopts just support single character option
for argument in $options
do
 index=`expr $index+1`
 case $argument in
    -winIn)WIT_INSTALL_PATH=${arguments[index]};;
    -winLc)WIT_LOCAL=${arguments[index]};;
    -winWb)WIT_SUPP=${arguments[index]};;
    -rserverWb)SHINY_SERVER=${arguments[index]};;
    -ldapDm)LDAP_DOMAIN=${arguments[index]};;
    -ldapPw)LDAP_ADMAIN_PASSWD=${arguments[index]};;
 esac
done

 if [ -z "$LDAP_DOMAIN" ] || [ -z "$LDAP_ADMAIN_PASSWD" ]
    then
     echo "LDAP DOMAIN and ADMIN PASSWORD is required ! "
     exit 1
 fi 


 grep "deb http://cran.rstudio.com/bin/linux/ubuntu trusty/" /etc/apt/sources.list
 
 if [ $? -ne 0 ]
   then
      echo "deb http://cran.rstudio.com/bin/linux/ubuntu trusty/" >> /etc/apt/sources.list
 fi

gpg --keyserver keyserver.ubuntu.com --recv-key E084DAB9
apt-get update
apt-get upgrade

echo "******************************************"
echo "check and install R with required packages"
echo "******************************************"
export R_LIBS_SITE=$R_LIBRARY
command -v R >/dev/null 2>&1
if [ $? -ne 0 ]
   then
   apt-get install r-base r-base-dev
fi

   apt-get install openjdk-7-*
   apt-get install r-cran-rjava
   apt-get -y build-dep libcurl4-gnutls-dev
   apt-get -y install libcurl4-gnutls-dev
   R CMD javareconf
   apt-get install mesa-common-dev
   apt-get install libglu1-mesa-dev
   apt-get install libcairo2-dev
  apt-get install libxml2-dev
##install shiny and required R packages
./RpackagesInstall.r
chmod 777 -R /usr/local/lib/




echo "******************************************"
echo "check and install LDAP server "
echo "******************************************"
TEMP_PATH=$PATH
PATH=$PATH:/usr/sbin
export PATH
command -v slapd
if [ $? -ne 0 ]
 then
 apt-get install slapd ldap-utils
fi
 dpkg-reconfigure slapd
 apt-get install ldapscripts

 grep "shiny ALL=NOPASSWD:/usr/sbin/ldapadduser" /etc/sudoers
 
 if [ $? -ne 0 ]
    then
      echo "shiny ALL=NOPASSWD:/usr/sbin/ldapadduser,/usr/sbin/ldapdeleteuser,/usr/sbin/ldapsetpasswd,/usr/sbin/ldapaddusertogroup,/usr/sbin/ldapdeleteuserfromgroup,/usr/sbin/ldapaddgroup" >> /etc/sudoers

 fi
 
 ORGIN_LDOMAIN=$LDAP_DOMAIN
 DOMAIN_ARR=(${LDAP_DOMAIN//./ })
 DOMAIN_TRANSMIT=("${DOMAIN_ARR[@]/#/dc=}")
 LDAP_DOMAIN=($(IFS=$"," ; echo "${DOMAIN_TRANSMIT[*]}"))
 echo "ldap_domain:  $LDAP_DOMAIN"
 
 grep "SERVER=localhost" /etc/ldapscripts/ldapscripts.conf
 
 if [ $? -ne 0 ]
    then
      echo "SERVER=localhost" >> /etc/ldapscripts/ldapscripts.conf
      echo  "BINDDN='cn=admin,$LDAP_DOMAIN'" >> /etc/ldapscripts/ldapscripts.conf
      echo 'BINDPWDFILE="/etc/ldapscripts/ldapscripts.passwd"' >> /etc/ldapscripts/ldapscripts.conf
      echo  "SUFFIX='$LDAP_DOMAIN'" >> /etc/ldapscripts/ldapscripts.conf
      echo "GSUFFIX='ou=Groups'" >> /etc/ldapscripts/ldapscripts.conf
      echo "USUFFIX='ou=Users'" >> /etc/ldapscripts/ldapscripts.conf
    else
      sed -i s/^BINDDN=.*/"BINDDN='cn=admin,$LDAP_DOMAIN'"/ /etc/ldapscripts/ldapscripts.conf
      sed -i s/^SUFFIX=.*/"SUFFIX='$LDAP_DOMAIN'"/ /etc/ldapscripts/ldapscripts.conf
 fi
 
 sh -c "echo -n $LDAP_ADMAIN_PASSWD > /etc/ldapscripts/ldapscripts.passwd"
 chmod 400 /etc/ldapscripts/ldapscripts.passwd

  
 sed -i "s/^dn: ou=Users.*/dn: ou=Users,$LDAP_DOMAIN/" Init_ldap.ldif
 sed -i "s/^dn: ou=Groups.*/dn: ou=Groups,$LDAP_DOMAIN/" Init_ldap.ldif
 sed -i "s/^dn: cn=witnessUsers.*/dn: cn=witnessUsers,ou=Groups,$LDAP_DOMAIN/" Init_ldap.ldif
 sed -i "s/^dn: cn=witnessManagers.*/dn: cn=witnessManagers,ou=Groups,$LDAP_DOMAIN/" Init_ldap.ldif
 sed -i "s/^dn: uid=witness.*/dn: cn=witness,ou=Users,$LDAP_DOMAIN/" Init_ldap.ldif
 sed -i "s/^dn: uid=admin1.*/dn: cn=admin1,ou=Users,$LDAP_DOMAIN/" Init_ldap.ldif

 ldapadd -x -D cn=admin,$LDAP_DOMAIN -W -f Init_ldap.ldif

 ldapadduser admin witnessManagers
 ldapaddusertogroup admin witnessManagers
 (echo "f5866c4a4d6014ecced47960c2e3d07f"; sleep 1;echo "f5866c4a4d6014ecced47960c2e3d07f")|sudo ldapsetpasswd admin

 ldapadduser witness witnessUsers
 ldapaddusertogroup witness witnessUsers
 (echo "c4aa831ed53f5fdaf0ef33138a0b404a"; sleep 1;echo "c4aa831ed53f5fdaf0ef33138a0b404a")|sudo ldapsetpasswd witness

 
 sleep 4

PATH=$TEMP_PATH
export PATH
echo "Ok............ "

echo "******************************************"
echo "check and deploy witness "
echo "******************************************"
 if [ -z $WIT_LOCAL ]
    then
        wget $WIT_SUPP
            if [  $? -ne 0 ]
                then
                 echo "Internet is not available!"
                 exit 1
           fi
        tar -xf ${WIT_SUPP##*/} -C $WIT_INSTALL_PATH
        rm ${WIT_SUPP##*/}
    else
        tar -xf ${WIT_LOCAL##*/} -C $WIT_INSTALL_PATH
 fi
 
 if [ ${WIT_INSTALL_PATH:0:1} != "/" ]
   then
   WIT_INSTALL_PATH=$(pwd)/$WIT_INSTALL_PATH
 fi
 
 chown shiny:shiny -R $WIT_INSTALL_PATH
 chmod 775 -R  $WIT_INSTALL_PATH 

 mkdir -p "/var/log/witness/logBackup"
 chown -R  shiny:shiny  $LOG_PATH
 echo "Ok............ "

echo "******************************************"
echo "check and install shiny-server"
echo "******************************************"
TEWIT_INSTALL_PATH=${WIT_INSTALL_PATH//\/./}
INWIT_INSTALL_PATH=${TEWIT_INSTALL_PATH////\\/}

command -v shiny-server >/dev/null 2>&1
if [ $? -ne 0 ]
   then
   apt-get install gdebi-core
   wget $SHINY_SERVER
   gdebi ${SHINY_SERVER##*/}
   rm ${SHINY_SERVER##*/}
fi
usermod -a -G root shiny
cp /etc/shiny-server/shiny-server.conf /etc/shiny-server/shiny-server.conf.backup
sed -i "s/directory_index.*//" /etc/shiny-server/shiny-server.conf
sed -i "s/auth_passwd_file.*//" /etc/shiny-server/shiny-server.conf
sed -i "s/site_dir.*/site_dir $INWIT_INSTALL_PATH\/service;/" /etc/shiny-server/shiny-server.conf

    DEP_WIT_LC=$(grep -n "location.*witness_updated.*" /etc/shiny-server/shiny-server.conf|grep -Eo '^[^:]+')


if [ -z "$DEP_WIT_LC" ]
    then
    line_num=$(grep -n "log_dir.*" /etc/shiny-server/shiny-server.conf|grep -Eo '^[^:]+')
    line_num=$(( $line_num+2 ))
    insertline="\t location /witness_updated { \n 
                \t template_dir $TEWIT_INSTALL_PATH/template; \n
                \t required_group witnessUsers witnessManagers;\n
                \t }\n
                \t location /admin { \n
                \t template_dir $TEWIT_INSTALL_PATH/template;\n
                \t required_group witnessManagers;\n
                \t }\n
                \t location /witnesslogin {\n
                \t template_dir $TEWIT_INSTALL_PATH/template;\n
                \t required_group witnessUsers witnessManagers;\n
                \t }\n"
    awk -v lnum="$line_num" -v insert="$insertline" 'NR==lnum{print insert} 1' /etc/shiny-server/shiny-server.conf > shinyserver.conf
   cp shinyserver.conf /etc/shiny-server/shiny-server.conf
   rm shinyserver.conf
    else
      sed -i "s/template_dir.*/template_dir $INWIT_INSTALL_PATH\/template;/" /etc/shiny-server/shiny-server.conf             
 fi
 
  DEP_AUTH_LOC=$(grep -n "auth_ldap.*" /etc/shiny-server/shiny-server.conf|grep -Eo '^[^:]+')
  if [ -z "$DEP_AUTH_LOC" ]
     then
     echo "auth_ldap ldap://localhost/$LDAP_DOMAIN {" >> /etc/shiny-server/shiny-server.conf
     echo "group_search_base ou=Groups;" >> /etc/shiny-server/shiny-server.conf
     echo "user_search_base ou=Users;" >> /etc/shiny-server/shiny-server.conf
     echo "group_filter \"memberUid={username}\";" >> /etc/shiny-server/shiny-server.conf
     echo "user_bind_template \"uid={username},ou=Users,{root}\";" >> /etc/shiny-server/shiny-server.conf
     echo "}"  >> /etc/shiny-server/shiny-server.conf
     else
      sed -i "s/auth_ldap.*/auth_ldap ldap:\/\/$ORGIN_LDOMAIN\/$LDAP_DOMAIN {/" /etc/shiny-server/shiny-server.conf
  fi

echo "Ok............ "


echo "******************************************"
echo "check and deploy security certificate"
echo "******************************************"
##If the administrator have a certificate leased by a CA,
##he just need to dieplace the witness 

command -v openssl
if [ $? -ne 0 ]
   then
   apt-get install openssl
fi 

SSL_SUBJ="/C=US/CN=$ORGIN_LDOMAIN"
openssl req -x509 -nodes -newkey rsa:2048 -keyout $SSL_PKEY -out $SSL_CERT -days 3650 -subj $SSL_SUBJ

    SSL_LC=$(grep -n "ssl /.*" /etc/shiny-server/shiny-server.conf|grep -Eo '^[^:]+')
if [ -z "$SSL_LC" ]
   then
    line_num=$(grep -n "listen.*3838;.*" /etc/shiny-server/shiny-server.conf|grep -Eo '^[^:]+')
    line_num=$(( $line_num+1 ))
    insertline="ssl $SSL_PKEY  $SSL_CERT;"
    awk -v lnum="$line_num" -v insert="$insertline" 'NR==lnum{print insert} 1' /etc/shiny-server/shiny-server.conf > shinyserver.conf
   cp shinyserver.conf /etc/shiny-server/shiny-server.conf
   rm shinyserver.conf
   else
   tem_ssl_key=${SSL_PKEY////\\/}
   tem_ssl_cert=${SSL_CERT////\\/}
   sed -i "s/^ssl \/.*/ssl $tem_ssl_key $tem_ssl_cert;/" /etc/shiny-server/shiny-server.conf 
fi

echo "Ok................"

echo "******************************************"
echo "check and install nginx"
echo "******************************************"

command -v nginx >/dev/null 2>&1

if [ $? -ne 0 ]
   then
   apt-get install nginx
fi

rm $NGINX_CONF

insertline="
            server {\n
            \t listen 80;\n
            \t #rewrite ^ https://\$host\$request_uri? permanent;\n
               \t keepalive_timeout     70;\n
            \t location / {\n
            \t root $TEWIT_INSTALL_PATH/service/main;\n
            \t index index.html index.htm;\n
            \t ssi   on;\n
            \t }\n

             }\n\n

               server {\n
            \t listen 443;\n
            \t ssl                   on; \n
            \t ssl_certificate       $SSL_CERT;\n
            \t ssl_certificate_key   $SSL_PKEY;\n
            \t keepalive_timeout     70;\n
            \t location / {\n
            \t root $TEWIT_INSTALL_PATH/service/main;\n
            \t index index.html index.htm;\n
            \t ssi   on;\n
            \t }\n
             }
            " 
echo -e $insertline > $NGINX_CONF

echo "Ok............................."

echo "******************************************"
echo "set crontab for log backup........."
echo "******************************************"
crontab -l > cron.temp

CRON_LOC=$(grep -n ".*backup_day.sh$" cron.temp|grep -Eo '^[^:]+')
if [ -z "$CRON_LOC" ]
   then
     echo "01 01 * * * $TEWIT_INSTALL_PATH/linux_script/backup_day.sh" >> cron.temp   
   else 
     sed -i "s/.*backup_day.sh$/01 01 * * * $INWIT_INSTALL_PATH\/linux_script\/backup_day.sh/" cron.temp 
fi
     crontab cron.temp
     rm cron.temp
# ( crontab -l 2>/dev/null ; echo "01 01 * * * $WIT_INSTALL_PATH/linux_script/backup_day.sh" ) | crontab -

echo "Ok......................."

echo "******************************************"
echo "restart servers........."
echo "******************************************"
service nginx restart
service shiny-server restart
