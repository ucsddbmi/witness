#!/bin/bash


wget -q --tries=20 --timeout=10 --spider "https://www.google.com"

if [  $? -ne 0 ]
   then
   echo "Internet is not available!"
   exit 1
fi

WIT_SUPP="witness2.ucsd-dbmi.org/Sources/witnessArc.tar"
WIT_LOCAL="."

options=$@
arguments=($options)
index=0
for argument in $options
do
 index=`expr $index+1`
 echo "index:$index   argu1:$argument   argu:${arguments[index]}"
 case "$argument" in
    -winLc) WIT_LOCAL=${arguments[index]}
              ;;
    -winWb) WIT_SUPP=${arguments[index]}
              ;;
 esac
done
temporary_fold="/tmp/temp_wit"
mkdir $temporary_fold

wget $WIT_SUPP -P $temporary_fold
     if [  $? -ne 0 ]
                then
                 echo "Internet is not available!"
                rm -r $temporary_fold 
                exit 1
      fi

backup_witness="$WIT_LOCAL"/witness_backup_`date +%Y-%m-%d `.tar
tar -cf $backup_witness "$WIT_LOCAL"/* 

tar -xf "$temporary_fold"/${WIT_SUPP##*/} -C $WIT_LOCAL
        rm -r $temporary_fold

