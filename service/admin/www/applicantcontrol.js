var applyusercount=0;
var showdeclinemenuCondition=true;
/*****************Decline dialog*********************/
$(document).ready(function(){
  var chpwdInv=setInterval(function(){chpwdshow()},0); 
    function chpwdshow(){        
        if($("#decapplicantdlg").length>0){
  $('#decapplicantdlg').dialog({
  autoOpen:false,
     modal:true,
     title:"Decline Applicant",
     buttons:{
                "Confirm":function(){
                  
                  $(this).dialog("close");
                Shiny.onInputChange('decapplydlgsure',applyusercount++);     
                },
                "Cancel":function(){
                  $(this).dialog("close");
                }         
               }
  });
   clearInterval(chpwdInv);                  //just execute once after the element exist i the dom
        }
      }
});

/**********************abind contextmenu to table******************************/
function generateApplicantMenu(){
/*  try{
if($('#applyusertable').find('table').find('tr').length>0){
 $('#applyusertable').find('table').find('tr').contextPopup({
          title: 'Control Menu',
          items: [
            {label:'Approve',      icon:'icons/plus25.png',      action:function() { Shiny.onInputChange('approveapplydlgsure',applyusercount++); } },
            {label:'Decline',   icon:'icons/remove11.png',    action:function() { if(showdeclinemenuCondition)$('#decapplicantdlg').dialog("open"); } }
          ]
        }); 
}}catch(e){
  alert(e.message);
}
*/
$('#applyusertable').find('table').find("tr").contextmenu({

autoTrigger:true,
menu:[
      {title:"Approve",uiIcon:"ui-icon-plusthick",action:function(event,ui) { 
       Shiny.onInputChange('approveapplydlgsure',applyusercount++);
        }},
        {title:"Decline",uiIcon:"ui-icon-closethick",action:function(event,ui) { 
         if(showdeclinemenuCondition)$('#decapplicantdlg').dialog("open");
        }}
      ]
});
}


Shiny.addCustomMessageHandler('showdeclineInfor',function(message){
    if(message==" "){
      showdeclinemenuCondition=false;
    }else{
         showdeclinemenuCondition=true;
         $('#declineConfirm').text(message); 
    }
});

Shiny.addCustomMessageHandler('applicantionAlert',function(message){
    alert(message);
});