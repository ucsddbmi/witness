
var adminCountTmp=0;
var showmenuCondition=true; 


$(document).ready(function(){  
                   var readonlyAdminprofileInv=setInterval(function(){readonlyAdminprofileshow()},0);
    function readonlyAdminprofileshow(){
        if($("#chrankDlgAdm").length>0){
                 $('#chrankDlgAdm').attr("readonly","readonly");
                 $('#chpwdDlgAdmin').attr("readonly","readonly");
                  clearInterval(readonlyAdminprofileInv); 
}
}
                }); 



$(document).ready(function(){
  
//////////////////////////////////////////////////////////
/*******add administrator dialog************/
  var addAdminInv=setInterval(function(){addadmindlgshow()},0); 
    function addadmindlgshow(){        
        if($("#addadmindlg").length>0){
  $('#addadmindlg').dialog({
  autoOpen:false,
     modal:true,
     title:"Add Administrator",
     height:410,
     width:330,
     buttons:{
                "Confirm":function(){
                  
                  $(this).dialog("close");
                Shiny.onInputChange('addnewadmindlgsure',adminCountTmp++);     
                },
                "Cancel":function(){
                  $(this).dialog("close");
                }         
               }
  });
   clearInterval(addAdminInv);                  //just execute once after the element exist i the dom
        }
}

/*****************delete administrator*********************/
  var dlAdminInv=setInterval(function(){deleteAdminshow()},0); 
    function deleteAdminshow(){        
        if($("#deleteadmindlg").length>0){
  $('#deleteadmindlg').dialog({
  autoOpen:false,
     modal:true,
     title:" Delete   Administrator  ",
     buttons:{
                "Sure":function(){
                  
                $(this).dialog("close");
                Shiny.onInputChange('removeadmindlgsure',communicateTmp++);     
                },
                "Cancel":function(){
                  $(this).dialog("close");
                }         
               }
  });
   clearInterval(dlAdminInv);                  //just execute once after the element exist i the dom
        }
      }

/********************** admin log dialog**********************/
$(document).ready(function(){
  var profileInv=setInterval(function(){profileshow()},0); 
    function profileshow(){        
        if($("#adminlogdlg").length>0){
  $('#adminlogdlg').dialog({
  autoOpen:false,
     modal:true,
     width:830,
     height:860,
     title:"Administrator activity log",
     buttons:{
                "OK":function(){               
                  $(this).dialog("close");  
                }
               },
     close: function(event,ui){Shiny.onInputChange('adminshowlogdlgclose',communicateTmp++);  }
  });
   clearInterval(profileInv);                  //just execute once after the element exist i the dom
   $("#adminlogdlg").siblings("div.ui-widget-header").addClass("userprofile_header");
   $("#adminlogdlg").parent("div.ui-dialog").addClass("userprofile_body");  
   $("#adminlogdlg").siblings("div.ui-dialog-buttonpane").addClass("userprofile_footer"); 
   $("#adminlogdlg").siblings("div.ui-dialog-buttonpane").find(".ui-button").addClass("userprofile_button"); 
     
        }
  
    }
      
});

/*****************change rank*********************/
$(document).ready(function(){
  var chrkInv=setInterval(function(){chrkshow()},0); 
    function chrkshow(){        
        if($("#changerankadmindlg").length>0){
  $('#changerankadmindlg').dialog({
  autoOpen:false,
     modal:true,
     title:"Change Privilege",
      height:350,
     width:330,
     buttons:{
                "Confirm":function(){
                  
                  $(this).dialog("close");
                Shiny.onInputChange('chrankdlgsure',communicateTmp++);     
                },
                "Cancel":function(){
                  $(this).dialog("close");
                }         
               }
  });
   clearInterval(chrkInv);                  //just execute once after the element exist i the dom
        }
      }
});
/*****************change passwd*********************/
var chAdminpwdInv=setInterval(function(){chAdminpwdshow()},0); 
    function chAdminpwdshow(){        
        if($("#changeadminpwddlg").length>0){
  $('#changeadminpwddlg').dialog({
  autoOpen:false,
     modal:true,
     title:"Change Passwd",
     height:330,
     width:330,
     buttons:{
                "Confirm":function(){
                  
                  $(this).dialog("close");
                Shiny.onInputChange('chadminpwddlgsure',communicateTmp++);     
                },
                "Cancel":function(){
                  $(this).dialog("close");
                }         
               }
  });
   clearInterval(chAdminpwdInv);                  //just execute once after the element exist i the dom
        }
      }

});


/***********for administrator management initialization*************/
/*var testadminrankIntv=setInterval(function(){testadminrank()},0);
function testadminrank(){
   Shiny.onInputChange('testrank',communicateTmp++); 
   clearInterval(testadminrankIntv);
}

var isSuperAdmin=false;
Shiny.addCustomMessageHandler('adminRankMsg',function(message){
  try{
       var Ranknum=parseInt(message); 
       if(Ranknum==1){
         isSuperAdmin=false; //true
       }else{
         isSuperAdmin=true;
       }
  }catch(e){
    alert(e.message);
  }    
});*/

/*******************************************************************/
function adminmenu(){
    /* $('#admintable').find('table').find('tr').contextPopup({
          title: 'Control Menu',
          items: [
            {label:'Add Administrator',      icon:'icons/plus25.png',      action:function() { $('#addadmindlg').dialog("open"); } },
            {label:'Remove Administrator',   icon:'icons/remove11.png',    action:function() { 
              if(showmenuCondition)
              {
               $('#deleteadmindlg').dialog("open") 
              } } },
            null, // divider
            {label:'Change Rank', icon:'icons/cogs3.png',       action:function() { $('#changerankadmindlg').dialog("open") } },
            {label:'Change Passwd', icon:'icons/font2.png',     action:function() { $('#changeadminpwddlg').dialog("open") } }
          ]
        }); */
        
$('#admintable').find('table').find("tr").contextmenu({

autoTrigger:true,
menu:[
       {title:"Administrator Activity Log",uiIcon:"ui-icon-plusthick",action:function(event,ui) { 
         Shiny.onInputChange('adminlogdlgshow',communicateTmp++);  
          $('#adminlogdlg').trigger("shown");
              $('#adminlogdlg').dialog("open"); 
        }},
      {title:"Add Administrator",uiIcon:"ui-icon-plusthick",action:function(event,ui) { 
         $('#addadmindlg').dialog("open");
        }},
        {title:"Remove Administrator",uiIcon:"ui-icon-closethick",action:function(event,ui) { 
         if(showmenuCondition)
          {
               $('#deleteadmindlg').dialog("open") 
          }
        }},
        {title:"----"},
        {title:"Change Privilege",uiIcon:"ui-icon-wrench",action:function(event,ui) { 
         $('#changerankadmindlg').dialog("open");
        }},
        {title:"Change Password",uiIcon:"ui-icon-key",action:function(event,ui) { 
         $('#changeadminpwddlg').dialog("open");
        }}
      ]
});
  
}


/*************************************************************/
$(document).ready(function(){
              try{
                    var passwordInputBinding = new Shiny.InputBinding();
                    $.extend(passwordInputBinding, {
                    find: function(scope) {
                    return $(scope).find('input[type=\"password\"]');
                    },
                    getId: function(el) {
                    return Shiny.InputBinding.prototype.getId.call(this, el) || el.name;
                    },
                    getValue: function(el) {
                    return el.value;
                    },
                    setValue: function(el, value) {
                    el.value = value;
                    },
                    subscribe: function(el, callback) {
                    $(el).on('keyup.passwordInputBinding input.passwordInputBinding', function(event) {
                    callback(true);
                    });
                    $(el).on('change.passwordInputBinding', function(event) {
                    callback(false);
                    });
                    },
                    unsubscribe: function(el) {
                    $(el).off('.passwordInputBinding');
                    },
                    receiveMessage: function(el, data) {
                    if (data.hasOwnProperty('value'))
                    this.setValue(el, data.value);
                    
                    if (data.hasOwnProperty('label'))
                    $(el).parent().find('label[for=' + el.id + ']').text(data.label);
                    
                    $(el).trigger('change');
                    },
                    getState: function(el) {
                    return {
                    label: $(el).parent().find('label[for=' + el.id + ']').text(),
                    value: el.value
                    };
                    },
                    getRatePolicy: function() {
                    return {
                    policy: 'debounce',
                    delay: 25
                    };
                    }
                    });
                    Shiny.inputBindings.register(passwordInputBinding, 'shiny.textInput');
                    
                    }catch(err){
                    alert(err.message);
                    };
                    
                   var passwdtypeInv=setInterval(function(){passwdtypeinit()},0); 
                   function passwdtypeinit(){        
                   if($("#addadminDlgpwd").length>0&&$("#chAdminpwdDlgpwd").length>0){
                       $('#addadminDlgpwd').attr('type','password');
                       $('#chAdminpwdDlgpwd').attr('type','password');
                       clearInterval(passwdtypeInv);                 
                        }
                    }
                    
                    
/****************************transactions between R and js********************************/
Shiny.addCustomMessageHandler('adminAlert',function(message){
    alert(message);
});

Shiny.addCustomMessageHandler('adminlogtriggershow',function(message){
              
             
});

Shiny.addCustomMessageHandler('showDeleteInfor',function(message){
    if(message==" "){
      showmenuCondition=false;
    }else{
         showmenuCondition=true;
         $('#deleteAdminConfirm').text(message); 
    }
});

Shiny.addCustomMessageHandler('passwdencrypt',function(message){
    if(message=="Chpd"){
      Shiny.onInputChange("chgcryptedpassedstart",communicateTmp++);
      Shiny.onInputChange('chgadminencryptedpassed',md5($("#chAdminpwdDlgpwd").val()+$("#chpwdDlgAdmin").val()));  
    }else{
      Shiny.onInputChange("addcryptedpassedstart",communicateTmp++);
      Shiny.onInputChange('addadminencryptedpassed',md5($("#addadminDlgpwd").val()+$("#addusrDlgAdm").val())); 
    }
});

Shiny.addCustomMessageHandler("adminlogSingle",function(message){
  
  /*if(message.trim()==""){
    alert("sorry, the user's log can not be found!");
    return;
  }*/
   $("#adminlogfile").html(message);
});

});                    
                    
