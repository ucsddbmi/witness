
/*****************************************dtsetgrpAccCtrl Table's call back'*********************************************/

var tmpcount=0;
var dt_varTbAddch=setInterval(varaddcheck,0);
function varaddcheck()
{
  try{
  var selectcount=0;
if($('#dtsetgrpAccCtrl').find('table').find('td').length>0)
{
$('#dtsetgrpAccCtrl').find('table').addClass('dtctrltable');
if($.trim($('#dtsetgrpAccCtrl').find('table').find('td:first').text())=='')return;
$('#dtsetgrpAccCtrl').find('table').find('td').each(function(i){
   if(i%3==1){
  datsetssvarib=$.trim($(this).siblings(':first').text());
  datsetssvarib_lim=datsetssvarib+'_limits';
  var_lim=grpDtDescriptList[dt_slteddatsets][datsetssvarib_lim][0];
  dat_strs_lim = var_lim.split('_'); 
  
  is_group_selected=grpDtTotal[dt_slteddatsets][dt_grpindex];
  is_varib_selected="N";
  row=-1;
  
  if(is_group_selected=="Y")
  {
  row=grpDtDescriptList[dt_slteddatsets]["group"].indexOf(selectedgroup);
  is_varib_selected=grpDtDescriptList[dt_slteddatsets][datsetssvarib][row];  
  }
  
  if(dat_strs_lim[0]=='n')
   {
    if(!$.isNumeric(dat_strs_lim[1])||!$.isNumeric(dat_strs_lim[2]))
    { 
      alert('slibar value error');
      return;
    }
    $(this).html('<label Visible="true">~</label><br><div></div>');
    //$(this).html('<div></div>');
    dat_currentvar_slid0=parseFloat(dat_strs_lim[1]);
    dat_currentvar_slid1=parseFloat(dat_strs_lim[2]);
    if(is_group_selected=="Y"&&is_varib_selected=="Y")
    {
      var_lim=grpDtDescriptList[dt_slteddatsets][datsetssvarib_lim][row];
      if($.trim(var_lim)==""){alert("var_lim error");}
      else{
        try{
          dat_var_lim=var_lim.split("_");
          dat_currentvar_slid0=parseFloat(dat_var_lim[1]);
          dat_currentvar_slid1=parseFloat(dat_var_lim[2]);
        }catch(err){
          var_lim=grpDtDescriptList[dt_slteddatsets][datsetssvarib_lim][0];
          dat_var_lim=var_lim.split("_");
          dat_currentvar_slid0=parseFloat(dat_var_lim[1]);
          dat_currentvar_slid1=parseFloat(dat_var_lim[2]);
          var_lim_strs="n_"+dat_currentvar_slid0+"_"+dat_currentvar_slid1;
          grpDtDescriptList[dt_slteddatsets][datsetssvarib_lim][row]=var_lim_strs; 
          Shiny.onInputChange('dt_slideValue',var_lim_strs);
        }
      }
    }
    var sliderminv=parseFloat(dat_strs_lim[1]);
    var slidermaxv=parseFloat(dat_strs_lim[2]);
    if(!isInt(sliderminv))sliderminv=parseFloat(sliderminv.toFixed(2));
    if(!isInt(slidermaxv))slidermaxv=parseFloat(slidermaxv.toFixed(2));
    if(!isInt(dat_currentvar_slid0))dat_currentvar_slid0=parseFloat(dat_currentvar_slid0.toFixed(2));
    if(!isInt(dat_currentvar_slid1))dat_currentvar_slid1=parseFloat(dat_currentvar_slid1.toFixed(2));
    $(this).find("label").html(""+dat_currentvar_slid0+"~"+dat_currentvar_slid1);
    
    $(this).find('div').slider({
      min: sliderminv,
      max: slidermaxv,
      value: [dat_currentvar_slid0,dat_currentvar_slid1],
      tooltip:false,
    
    }).on("slideStop",function(ev){
        datsetssvarib=$.trim($(this).parent().parent().siblings(':first').text());
        datsetssvarib_lim=datsetssvarib+'_limits';
        is_group_selected=grpDtTotal[dt_slteddatsets][dt_grpindex];
        is_varib_selected="N";
        row=-1;
        upboard=this.value[0];
        endboard=this.value[1];
        if(!isInt(upboard))upboard=upboard.toFixed(2);
        if(!isInt(endboard))endboard=endboard.toFixed(2);
        
        $(this).parent().siblings("label").html(""+upboard+"~"+endboard);
        if(is_group_selected=="Y")
          {
            row=grpDtDescriptList[dt_slteddatsets]["group"].indexOf(selectedgroup);
            is_varib_selected=grpDtDescriptList[dt_slteddatsets][datsetssvarib][row];  
          }  
         if(is_group_selected=="Y"&&is_varib_selected=="Y")
          {
            var_lim_strs="n_"+upboard+"_"+endboard;
            grpDtDescriptList[dt_slteddatsets][datsetssvarib_lim][row]=var_lim_strs; 
            Shiny.onInputChange('dt_slideValue',var_lim_strs);
          }else {
            //alert();
            alert("please select datasets and variable!");
          }
   // alert(JSON.stringify(this.value));
    });
   }else{
     var varid="dtvarselectid"+selectcount;
     selectcount++;
     var sel="<div><select id="+varid+" multiple='multiple'></select></div>";
     //alert(sel);
     $(this).html(sel);
     var multioptions="";
     for(var i=1;i<dat_strs_lim.length;i++)
     {
       multioptions+="<option value='"+dat_strs_lim[i]+"'>"+dat_strs_lim[i]+"</option>";
     }
     $(this).find("select").append(multioptions);
     $(this).find("select").multipleSelect({
       placeholder: "Please Choose",
       onClose:function(){
         //alert($(this)..multipleSelect("getSelects").length);
        //datsetssvarib=$.trim($("#"+varid).parent().parent().siblings(':first').text());
        datsetssvarib=$("#"+varid).parent().parent().siblings(':first').text();
        datsetssvarib_lim=datsetssvarib+'_limits';
        is_group_selected=grpDtTotal[dt_slteddatsets][dt_grpindex];
        is_varib_selected="N";
        row=-1;
        if(is_group_selected=="Y")
          {
            row=grpDtDescriptList[dt_slteddatsets]["group"].indexOf(selectedgroup);
            is_varib_selected=grpDtDescriptList[dt_slteddatsets][datsetssvarib][row];  
          }  
        if(is_group_selected=="Y"&&is_varib_selected=="Y")
          {
            mulSelectedCh="c";
            selectedChex=$("#"+varid).multipleSelect("getSelects"); 
            for(var i=0;i<selectedChex.length;i++)
            {
              mulSelectedCh+="_"+selectedChex[i];
            }
            //alert(mulSelectedCh);
            if(selectedChex.length>0){
               grpDtDescriptList[dt_slteddatsets][datsetssvarib_lim][row]=mulSelectedCh; 
               Shiny.onInputChange('dt_multipleSelValues',mulSelectedCh);
            }
          }
        else{
           alert("please select datasets and variable!");
           $("#"+varid).multipleSelect('setSelects',[]);
         }
       }
     });
     if(is_group_selected=="Y"&&is_varib_selected=="Y")
    {
      var_lim=grpDtDescriptList[dt_slteddatsets][datsetssvarib_lim][row];
      if($.trim(var_lim)!=""){
       dat_var_lim=var_lim.split("_");
       $("#"+varid).multipleSelect('setSelects',dat_var_lim.slice(1,dat_var_lim.length));
      }
    }
    
   }
//$(this).html('<input type=\"range\" min=\"1001\" max=\"1200\"></input>');

//$(this).html('<select multiple=\"multiple\"><option value=\"volvo\">No</option><option value=\"saab\">Yes</option></select>');

}else if(i%3==2){        
    $(this).html('<input type=\"checkbox\" ></input>');
     datsetssvarib=$.trim($(this).siblings(':first').text());
    is_group_selected=grpDtTotal[dt_slteddatsets][dt_grpindex];
    row=-1;
    if(is_group_selected=="Y")
      {
        row=grpDtDescriptList[dt_slteddatsets]["group"].indexOf(selectedgroup);
        
        if(grpDtDescriptList[dt_slteddatsets][datsetssvarib][row]=="Y")
         {
           $(this).find("input").attr('checked',true);
         }  
      }
    
    $(this).find('input').click(function(){
    datsetssvarib=$.trim($(this).parent().siblings(':first').text());
    datsetssvarib_lim=datsetssvarib+'_limits';
    is_group_selected=grpDtTotal[dt_slteddatsets][dt_grpindex]; 
    row=grpDtDescriptList[dt_slteddatsets]["group"].indexOf(selectedgroup);
    if($(this).attr('checked')=='checked')
      {
        
        // grpDtTotal[$.trim($(this).parent().siblings('td').text())][dt_grpindex]='N';
        if(is_group_selected=="Y")
         {
          //alert(grpDtDescriptList[dt_slteddatsets][datsetssvarib][row]);
          grpDtDescriptList[dt_slteddatsets][datsetssvarib][row]="N";  
          checkvar_lim=grpDtDescriptList[dt_slteddatsets][datsetssvarib_lim][0].split("_");
          if(checkvar_lim[0]=="n")
           {
           // $(this).parent().siblings(":last").find(".slider").slider('setValue',checkvar_lim.slice(1,checkvar_lim.length));             
            //  $(this).parent().siblings(":last").find(".slider").data("slider",checkvar_lim.slice(1,checkvar_lim.length));
           }
          else{
            $(this).parent().siblings(':last').find("select").multipleSelect('setSelects',[]);
           }
           
           $(this).removeAttr('checked');
          Shiny.onInputChange('dt_dtSingleVarAccess',$.trim($(this).parent().siblings(':first').text()));
          Shiny.onInputChange('dt_dtSingleVarAccessTriger',tmpcount++);
         }else{
           $(this).removeAttr('checked');
          alert("please make sure the group can access the dataset!");
         }       
      }
      else{
        if(is_group_selected=="Y")
         {
          //alert(grpDtDescriptList[dt_slteddatsets][datsetssvarib][row]);
          grpDtDescriptList[dt_slteddatsets][datsetssvarib][row]="Y";  
          checkvar_lim=grpDtDescriptList[dt_slteddatsets][datsetssvarib_lim][0].split("_");
          if(checkvar_lim[0]=="n")
           {
            
            //$(this).parent().siblings(":last").find(".slider").slider('setValue',checkvar_lim.slice(1,checkvar_lim.length)); 
           // console.log('New value is ' + currentValue);
           // $(this).parent().siblings(":last").find(".slider").slider("refresh");
             //$(this).parent().siblings(":last").find(".slider").setValue(checkvar_lim.slice(1,checkvar_lim.length));
           }
          else{
            $(this).parent().siblings(':last').find("select").multipleSelect('setSelects',checkvar_lim.slice(1,checkvar_lim.length));
           }
          $(this).attr('checked',true);  
          grpDtDescriptList[dt_slteddatsets][datsetssvarib_lim][row]=grpDtDescriptList[dt_slteddatsets][datsetssvarib_lim][0]; 
          Shiny.onInputChange('dt_dtSingleVarAccess',$.trim($(this).parent().siblings(':first').text()));
          Shiny.onInputChange('dt_dtSingleVarAccessTriger',tmpcount++);
        
         }else{
          alert("please make sure the group can access the dataset!");
          $(this).removeAttr('checked');
         } 
        
        }
    })
}
});
clearInterval(dt_varTbAddch);
}
}
catch(err){
  alert(err.message);
}
}

/*********************************check Integer**********************************************************/
 
function isInt(value) {
  return !isNaN(value) && (function(x) { return (x | 0) === x; })(parseFloat(value))
}

function updateFileinputElem(x){
          var el = $("#" + x);
          el.replaceWith(el = el.clone(true));
          var id = "#" + x+ "_progress";     
          $(id).css("visibility", "hidden");
}
/*******************************dataset upload dialog****************************************/
$(document).ready(function(){
  
    var uploaddtInv=setInterval(function(){uploaddtshow()},0); 
    function uploaddtshow(){        
    if($("#uploaddtdlg").length>0){
          $("#uploaddtdlg").dialog({
              autoOpen:false,
              modal:true,
              height:300,
              width:500,
              title:'Upload Dataset',
              show: {
                 effect:"blind",
                 duration:1
               },
               hide:{
                 effect:"explode",
                 duration:1
               },
               buttons:{
                "Confirm":function(){
                Shiny.onInputChange('UploadDatSetsure',tmpcount++);     
                },
                "Cancel":function(){
                  updateFileinputElem("uploaddata")
                  $(this).dialog("close");
                }         
               }
         });
         clearInterval(uploaddtInv); 
       }
    $("#uploaddtdlg").css({"overflow":"visible"});
      } 
/*******************************dataset delete dialog*************************************/     
    var deletedtInv=setInterval(function(){deletedtshow()},0); 
    function deletedtshow(){        
    if($("#deletedtdlg").length>0){
          $("#deletedtdlg").dialog({
              autoOpen:false,
              modal:true,
              height:200,
              width:500,
              title:'Delete Dataset',
              show: {
                 effect:"blind",
                 duration:1
               },
               hide:{
                 effect:"explode",
                 duration:1
               },
               buttons:{
                "Confirm":function(){  
                 Shiny.onInputChange('DeleteDatSetsure',tmpcount++); 
                 $(this).dialog("close");    
                },
                "Cancel":function(){
                  $(this).dialog("close");
                }         
               }
         });
         clearInterval(deletedtInv);
       }
        $("#deletedtdlg").css({"overflow":"visible"});
    }  
      
/*******************************dataset description upload dialog*************************************/     
    var datasetdcptInv=setInterval(function(){uploaddtdcptdilgshow()},0); 
    function uploaddtdcptdilgshow(){        
    if($("#uploaddcptdlg").length>0){
          $("#uploaddcptdlg").dialog({
              autoOpen:false,
              modal:true,
              height:350,
              width:500,
              title:'Upload Description file',
              show: {
                 effect:"blind",
                 duration:1
               },
               hide:{
                 effect:"explode",
                 duration:1
               },
               buttons:{
                "Confirm":function(){  
                 Shiny.onInputChange('uploadDtDcptsure',tmpcount++);     
                },
                "Cancel":function(){
                  $(this).dialog("close");
                  Shiny.onInputChange('uploadDtDcptcancle',tmpcount++); 
                }         
               }
         });
         clearInterval(datasetdcptInv);
       }
        $("#uploaddcptdlg").css({"overflow":"visible"});
    } 

      
/*******************************dataset description show dialog***********************************/     
    var datasetdcptshInv=setInterval(function(){uploaddtdcptshow()},0); 
    function uploaddtdcptshow(){        
    if($("#showdcptdlg").length>0){
          $("#showdcptdlg").dialog({
              autoOpen:false,
              modal:true,
              height:830,
              width:860,
              title:'Description file',
              show: {
                 effect:"blind",
                 duration:1
               },
               hide:{
                 effect:"explode",
                 duration:1
               },
               buttons:{
                "ok":function(){   
                 $(this).dialog("close");    
                }       
               }
         });
         clearInterval(datasetdcptshInv);
          $("#showdcptdlg").siblings("div.ui-widget-header").addClass("userprofile_header");
          $("#showdcptdlg").parent("div.ui-dialog").addClass("userprofile_body");  
          $("#showdcptdlg").siblings("div.ui-dialog-buttonpane").addClass("userprofile_footer"); 
          $("#showdcptdlg").siblings("div.ui-dialog-buttonpane").find(".ui-button").addClass("userprofile_button"); 
       }
    } 
    
});

///////////////////////////////datasets table contmenu/////////////////////////////////////////////////
function genDtsConTmenu(){
  if($('#datasets').find('table').find('td').length==0)return;
  $('#datasets').find('table').find("tr").contextmenu({
autoTrigger:true,
menu:[
      {title:"Dataset Discription",uiIcon:"ui-icon-person",action:function(event,ui) {
         Shiny.onInputChange('showdcptdlgtriger',tmpcount++);
         $('#showdcptdlg').trigger("shown");
         $('#showdcptdlg').dialog("open");
        }},
      {title:"Upload Dataset",uiIcon:"ui-icon-arrowthickstop-1-n",action:function(event,ui) { 
         $('#uploaddtdlg').dialog("open");
        }},
        {title:"Upload Description File",uiIcon:"ui-icon-arrowthickstop-1-n",action:function(event,ui)          
        { 
         //$('input[name="Comma"]').attr("checked","checked");
         $('#uploaddcptdlg').dialog("open");
        }},
        {title:"Delete Dataset",uiIcon:"ui-icon-closethick",action:function(event,ui) { 
         if($('#deldtsetString').text().trim().length===0)return;
         $('#deletedtdlg').dialog("open");
        }}
      ]
});
}


///////////////////////////////data variables table contmenu/////////////////////////////////////////////
function gendtsetgrpAccConTmenu(){
  if($('#dtsetgrpAccCtrl').find('table').find('td').length==0)return;
  $('#dtsetgrpAccCtrl').find('table').find("tr").contextmenu({
autoTrigger:true,
menu:[ 
      {title:"To Numeric Sliderbar",uiIcon:"ui-icon-seek-next",action:function(event,ui) {
        Shiny.onInputChange('changeDtvarpropnum',tmpcount++); 
        }},
      {title:"To Categorical List",uiIcon:"ui-icon-seek-prev",action:function(event,ui) { 
        Shiny.onInputChange('changeDtvarpropcat',tmpcount++); 
        }}
      ]
});
}



///////////////////////////////////////////////////////////////////////////////////////////
$(document).ready(function(){
Shiny.addCustomMessageHandler("dtdescript",function(message){
  //$("#dt_dtsetDescription").position=[event.pageX-20,event.pageY-20];
  if($.trim(message)=='open')
   {
     // $('#dt_dtsetDescription').dialog("option","position",[event.pageX,event.pageY]);
      $('#dt_dtsetDescription').dialog("open");
   }else{
      $('#dt_dtsetDescription').dialog("close");
   }
});

Shiny.addCustomMessageHandler("closeuploaddtdlg",function(message){
 $('#uploaddtdlg').dialog("close");
});

Shiny.addCustomMessageHandler("uploaderror",function(message){
 $("#UploadcheckSting").text(message);
 if(message.trim()!=""){updateFileinputElem("uploaddata")}
});

Shiny.addCustomMessageHandler("updategrpDtsets",function(message){
 grpDtTotal=JSON.parse(message);
 updateFileinputElem("uploaddata")
});

Shiny.addCustomMessageHandler("updatedatsetList",function(message){
 grpDtDescriptList=JSON.parse(message);
});

Shiny.addCustomMessageHandler("uploaddcpterror",function(message){
 $("#UploaddcptcheckSting").text(message);
});
Shiny.addCustomMessageHandler("uploaddcptupdate",function(message){
 updateFileinputElem("uploaddcpt");
});


Shiny.addCustomMessageHandler("uploaddcptclose",function(message){
 $("#uploaddcptdlg").dialog("close");
});

Shiny.addCustomMessageHandler("datacontrolAlert",function(message){
 alert(message);
});

});
/*////////////////////////for dataset control//////////////////////////////////////////////*/
var dt_grpindex=-1;
var selectedgroup="";
Shiny.addCustomMessageHandler('dt_grpselect',function(message){
    dt_grpindex=grpDtTotal.Group.indexOf(message);
    selectedgroup=message;
});

Shiny.addCustomMessageHandler('dtGroupDtUpdate',function(message){
    grpDtTotal=JSON.parse(message);
});

var dt_slteddatsets="";
Shiny.addCustomMessageHandler('dt_selecteddatsets',function(message){
    dt_slteddatsets=message;
});


Shiny.addCustomMessageHandler('dt_selectedDtChangetoJs',function(message){
     // alert(JSON.parse(message)['group']);
     // alert(dt_slteddatsets+"~~"+message);
     grpDtDescriptList[dt_slteddatsets]=JSON.parse(message);
});
