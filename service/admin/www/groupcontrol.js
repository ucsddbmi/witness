//////////////////////////////////////////////////////////

rightclicktmp=0;

function updateFileinputElem(x){
          var el = $("#" + x);
          el.replaceWith(el = el.clone(true));
          var id = "#" + x+ "_progress";     
          $(id).css("visibility", "hidden");
}

/*******************************************add group***********************************************/

$(document).ready(function(){
   var agmyInv=setInterval(function(){agshow()},0); 
    function agshow(){        
        if($("#addgrpdlg").length>0){
          $("#addgrpdlg").dialog({
          autoOpen:false,
              modal:true,
              draggable:true,
              title:'Add Group',
              width: 400,
              buttons:{
                 "Confirm":function(){
                  Shiny.onInputChange('addgrpsure',rightclicktmp++);
                },
                "Cancel":function(){
                  $(this).dialog("close");
                }
              },
              show: {
                 effect:"blind",
                 duration:400
               },
               hide:{
                 effect:"explode",
                 duration:400
               }
          }); 
          clearInterval(agmyInv);                  //just execute once after the element exist i the dom
        }
      }

});

/*****************************************delete confirm dialog*************************************/
$(document).ready(function(){
  var rgmyInv=setInterval(function(){rgshow()},0); 
    function rgshow(){        
        if($("#rmgrpconfirmdlg").length>0){
  $('#rmgrpconfirmdlg').dialog({
  autoOpen:false,
     modal:true,
     title:"Delete Group !",
   buttons:{
                "Confirm":function(){
                  
                  $(this).dialog("close");
                Shiny.onInputChange('Deletesure',rightclicktmp++);     
                },
                "Cancel":function(){
                  $(this).dialog("close");
                }         
               }
  });
   clearInterval(rgmyInv);                  //just execute once after the element exist i the dom
        }
      }
  //$("#alertdlg").siblings(".ui-widget-header").attr("background","yellow");
  // alert($("#alertdlg").siblings(".ui-widget-header").html());
 // $('#rmgroupconfirm').click(function(){
  //  $("#confms").html("dssdfd");
  //$('#confirmdlg').dialog("open");
  //});
});
/******************************************alert dialog*****************************************************/
 $(document).ready(function(){
   var altmyInv=setInterval(function(){altshow()},0); 
    function altshow(){        
        if($("#alertdlg").length>0){
  $('#alertdlg').dialog({
     autoOpen:false,
     modal:true,
     title:"Alert!",
      buttons:{
                "Ok":function(){                
                  $(this).dialog("close");   
                }              
              }
  });
   clearInterval(altmyInv);                  //just execute once after the element exist i the dom
        }
      }
});


/************************************upload resources dialog**********************************************/
 $(document).ready(function(){
   var uploadInv=setInterval(function(){altshow()},0); 
    function altshow(){        
  if($("#uploaddlg").length>0){
     $('#uploaddlg').dialog({
     autoOpen:false,
     modal:true,
     minWidth:500,
     title:"Upload Files",
     buttons:{
                "Confirm":function(){            
                 Shiny.onInputChange('uploadsure',rightclicktmp++); 
                 $('#uploadsourceprocessimg').show();
                },
                "Cancel":function(){
                  updateFileinputElem("uploadFile")
                 $(this).dialog("close");
                }         
               },
    beforeClose:function(event,ui){
        Shiny.onInputChange('uploadcancel',rightclicktmp++); 
    },
    open:function(event,ui){
       $('#showUploadwarn').text(" ");
       Shiny.onInputChange('uploaddialogopen',rightclicktmp++); 
    }          
  });
   clearInterval(uploadInv);                  //just execute once after the element exist i the dom
    }
  }
});

/******************************************delete resources dialog*****************************************************/
 $(document).ready(function(){
   var deleteSourceInv=setInterval(function(){altsourcesshow()},0); 
    function altsourcesshow(){        
    if($("#removesourceloaddlg").length>0){
     $('#removesourceloaddlg').dialog({
     autoOpen:false,
     modal:true,
     minWidth:500,
     title:"Remove Files",
      buttons:{
                "Confirm":function(){
                Shiny.onInputChange('removesourcesure',rightclicktmp++);   
                $('#removesourceprocessimg').show();
                },
                "Cancel":function(){
                  $(this).dialog("close");
                }         
               }
  });
   clearInterval(deleteSourceInv);                  //just execute once after the element exist i the dom
        }
      }
});

/******************************************FeatureTree DataMainUI unselect dialog*****************************************************/
 $(document).ready(function(){
   var ftTrUnselectInv=setInterval(function(){ftTrUnselectshow()},0); 
    function ftTrUnselectshow(){        
    if($("#ftTrUnselectAlert").length>0){
     $('#ftTrUnselectAlert').dialog({
     autoOpen:false,
     modal:true,
     minWidth:500,
     title:"Unselect Caution",
      buttons:{
                "Confirm":function(){
                  if(zTreeObj!=null){
                    zTreeObj.checkAllNodes(false);
                  }
                  $(this).dialog("close");
                },
                "Cancel":function(){
                  $(this).dialog("close");
                }         
               }
  });
   clearInterval(ftTrUnselectInv);                  //just execute once after the element exist i the dom
        }
      }
});

/*************************************feature Tree**************************************/
  var zTreeObj=null;
  ft_selectedGrp=" ";
  var nodes=null;
  var isparentNode=false;
$(document).ready(function(){
  try{    
  function treeFeatureDiscrip(event, treeId, treeNode)
  {
    if($.isEmptyObject(treeNode))return;    
      //alert(treeNode.tId.split("_")[1]);
       Shiny.onInputChange('ft_showDescription',treeNode.tId.split("_")[1]);   
  }
function treeRecordCheckInfo(event, treeId, treeNode){
  try{
      //alert("tId"+treeNode.tId+"name:"+treeNode.name+"ischecked:"+treeNode.checked); 
  //  var checkedjsonstr="["+treeNode.tId+","+(treeNode.checked?1:0)+"]";
     // var checkedNodes=zTreeObj.getCheckedNodes(true);
      tmpGrpFt='{"record": [';
      for(var i=0;i<nodes.length;i++)
      { 
        if(i==0){
        if(nodes[i].checked)tmpGrpFt=tmpGrpFt+'"Y"';
        else tmpGrpFt=tmpGrpFt+'"N"';
        }
        else{
        if(nodes[i].checked)tmpGrpFt=tmpGrpFt+',"Y"';
        else tmpGrpFt=tmpGrpFt+',"N"';
        }
      }
      tmpGrpFt=tmpGrpFt+']}';
      ft_grpFtRecord[ft_selectedGrp]=JSON.parse(tmpGrpFt).record;
      Shiny.onInputChange('treeCheckRecord',tmpGrpFt);
   }catch(err){
     alert(err.message);
   }
 }
function treeCheckCondition(treeId, treeNode){
   try{
    if(ft_selectedGrp==" "){
      alert("please select a group!");
      return false;
    }
    //the follow setence is unsafe,,,,the name of 'DataMainUI'
    if(treeNode.name!="DataMainUI"&&zTreeObj.getNodeByParam("name","DataMainUI", null).checked==false)
    {
      alert("Dataset should be accessed at first,please check 'DataMainUI'."); 
      return false;
    }else if(treeNode.name=="DataMainUI"&&zTreeObj.getNodeByParam("name","DataMainUI", null).checked==true){
      $("#ftTrUnselectAlertString").text("Please confirm to unselect the data feature. All the associated features will be unselected.");
      $("#ftTrUnselectAlert").dialog("open");
      return false;
    }
   }catch(err){
     alert(err.message);
     return false;
   }
    return true;
  }
function treeCodeUpLoad(event,treeId,treeNode){
  if($.isEmptyObject(treeNode))return;    
      //alert(treeNode.tId.split("_")[1]);
       Shiny.onInputChange('ft_showDescription',treeNode.tId.split("_")[1]);   
}
  
var setting={
         check: {
                enable: true
                },
         data:  {
               simpleData:{
                   enable:true 
                  }
                },
         callback: {
           onMouseUp:treeFeatureDiscrip,
           onMouseDown:treeCodeUpLoad,
           beforeCheck:treeCheckCondition,
           onCheck:treeRecordCheckInfo
         }             
};
  
/*function menu(){
  try{
if($('#usertable').find('table').find('tr').length>0){
 
}}catch(e){
  alert(e.message);
}
}*/
    
function createFtTree(){
  
  if($("#featureTree").length==0)return;
  var length=0;
  var treeItems=new Array();  
  for(var item in ft_treeData)
  {
    treeItems[length]=item;
    length++;
  }
  //$('#grouptable').find('.dataTables_scrollBody')
  
  //alert(length);
  var zTreeNodes="[";
  if(length!=3)
  {
    alert("tree data error!");
  }else{
   var zTreeNodes="[";
   for(var i=0;i<ft_treeData[treeItems[0]].length;i++){
    zTreeNodes=zTreeNodes+"{ id:"+(i+1)+",pId:"+ft_treeData[treeItems[0]][i]+",name:"+"'"+ft_treeData[treeItems[1]][i]+"'"+",open:true"+"}";
    if(i!=ft_treeData[treeItems[0]].length-1)zTreeNodes=zTreeNodes+",";
    }
    zTreeNodes=zTreeNodes+"]";
    //alert(zTreeNodes);
   
     zTreeObj=$.fn.zTree.init($("#featureTree"),setting,eval(zTreeNodes));
     nodes=zTreeObj.transformToArray(zTreeObj.getNodes());
   }
        
$('#featureTree').find("li").contextmenu({

autoTrigger:true,
menu:[
      {title:"Upload",uiIcon:"ui-icon-arrowthickstop-1-n",action:function(event,ui) { 
       $('#uploaddlg').dialog("open");
        }},
        {title:"Remove",uiIcon:"ui-icon-arrowthickstop-1-n",action:function(event,ui) { 
           Shiny.onInputChange('removemenustart',rightclicktmp++); 
        }}
      ]
});
          
clearInterval(ft_createTreeInverval);
}
  
 }catch(err){
   alert(err.message);
}


var ft_createTreeInverval=setInterval(createFtTree,0);


/*******************************shiny communicate with js******************************/

//$(document).ready(function(){
  /////////////9.13/////////// 

Shiny.addCustomMessageHandler("uploadvalidatemsg",function(message){
         $('#showUploadwarn').text(message);
          if(message.trim()){updateFileinputElem("uploadFile")}
});

Shiny.addCustomMessageHandler("removesourcemsg",function(message){
         $("#removesourcewarn").text(message);
         $('#removesourceloaddlg').dialog("open");
});

Shiny.addCustomMessageHandler("uploadprocessmsg",function(message){
         $('#showUploadwarn').html("<span style='color:black;'>"+message+"</span>");
});

Shiny.addCustomMessageHandler("removesourceprocessmsg",function(message){
         $("#removesourcewarn").html(message);
});

Shiny.addCustomMessageHandler("uploaddlgclosemsg",function(message){
           $('#uploaddlg').dialog("close");
           $('#uploadsourceprocessimg').hide();
           updateFileinputElem(message)
           
});
Shiny.addCustomMessageHandler("removesourcedlgclosemsg",function(message){
        $('#removesourceloaddlg').dialog("close");
        $('#removesourceprocessimg').hide();
});
//Shiny.addCustomMessageHandler("pagereload",function(message){
//   history.go(0);
//});
Shiny.addCustomMessageHandler("ft_updategrpftTab",function(message){
   ft_grpFtRecord=JSON.parse(message);
}); 

Shiny.addCustomMessageHandler("ft_updateftTab",function(message){
  ft_treeData=JSON.parse(message);
}); 
  
Shiny.addCustomMessageHandler("redrawfeatureTree",function(message){
  createFtTree();
});   
  
Shiny.addCustomMessageHandler("rmGrp_dialogConfirmButton",function(message){
   Shiny.onInputChange('RmTheGrp_dialogConfirmButton',message);
});
  ///////////////////////////
Shiny.addCustomMessageHandler("Alert",function(message){
  $("#alertms").html(message);
  $('#alertdlg').dialog("open");
});

Shiny.addCustomMessageHandler("Confirm",function(message){
   $('#rmgrpconfirmdlg').dialog("open");
});
Shiny.addCustomMessageHandler("deleteConfirmMsg",function(message){
   $("#confms").html(message);
});

Shiny.addCustomMessageHandler("addgrpopen",function(message){
   $('#adderror').html(message);
   $('#addgrpdlg').dialog("open");
});
Shiny.addCustomMessageHandler("addgrpclose",function(message){
   $('#addgrpdlg').dialog("close");
});

Shiny.addCustomMessageHandler("UpdateGrpDtTotalAfterAdd",function(message){
Shiny.onInputChange('UpdateGrpDt_add',rightclicktmp++);
});

Shiny.addCustomMessageHandler("featureAddorRmUpdate",function(message){
   ft_grpFtRecord=JSON.parse(message);
});

Shiny.addCustomMessageHandler("selectedGrp",function(message){
   try{ 
     if($.isEmptyObject(message)||message==""||$.isEmptyObject(zTreeObj))return;
    ft_selectedGrp=message;
   if(ft_selectedGrp==" ")
   {
     zTreeObj.checkAllNodes(false);  
   }else{     
     if($.inArray(ft_selectedGrp,Object.keys(ft_grpFtRecord))>=0){
     var nodes=zTreeObj.transformToArray(zTreeObj.getNodes());
     //alert(nodes.length);
     for(var i=0;i<nodes.length;i++)
     {
       if(ft_grpFtRecord[ft_selectedGrp][i]=="Y"&&nodes[i].isParent==false)
       {
         zTreeObj.checkNode(nodes[i],true,true);
         //alert(nodes[i].tId);
       }else{
         zTreeObj.checkNode(nodes[i],false,true);
       }
     }
     }else{
     alert("group not in groupfeature table!");
     } 
   }
   
   }catch(err){
         alert(err.message); 
     }
});
});

/******************************menu operation*************************************************/


function generateGrpmenu(){
   try{
if($('#grouptable').find('table').find('tr').length>0){
 /*$('#grouptable').find('table').find('tr').contextPopup({
          title: 'User Control Menu',
          items: [
            {label:'Add Group',      icon:'icons/plus25.png',      action:function() { $('#addgrpdlg').dialog("open"); } },
            {label:'Remove Group',   icon:'icons/remove11.png',    action:function() { $('#rmgrpconfirmdlg').dialog("open"); } }
          ]
        }); */
        
$('#grouptable').find('table').find("tr").contextmenu({

autoTrigger:true,
menu:[
      {title:"Add Group",uiIcon:"ui-icon-plusthick",action:function(event,ui) { 
      $('#addgrpdlg').dialog("open");
        }},
        {title:"Remove Group",uiIcon:"ui-icon-closethick",action:function(event,ui) { 
         $('#rmgrpconfirmdlg').dialog("open");
        }}
      ]
});     
        
}}catch(e){
  alert(e.message);
}
}
/*********************generate datatable use jquery api  *************************************/
/*$(document).ready(function(){
  var myInv=setInterval(function(){show()},1);    // wait for the element exist in the dom
      function show(){
        if($('#testtable').length==0)alert("no ele");
        
        if($('#testtable').length>0){
          //alert("ele show !");
          $('#testtable').DataTable({
           
            bFilter: true,
            bSort : true,
            bSortClasses: true,
            bAutoWidth: true
            });
          clearInterval(myInv);                  //just execute once after the element exist i the dom
        }
      }
 //$('#testtable').dataTable();
 // $('#testtable').dataTable().fnAddData(['sdf','sdfds','ssssss']);
 // alert(oTable.fnVersionCheck('1.10.4'));
});

*/

/*********************************************************************************************************************/
//var tabrows=document.getElementById('grouptable').childNodes[0].getElementsByTagName('table')[0].getElementsByTagName('tr');
/* for(var i=2;i<tabrows.length;i=i+1)
  {
   tabrows[i].onclick=function(){
   tbtd=this.getElementsByTagName('td');
   for(var j=1;j<=tbtd.length;j=j+1)
   {
    // document.getElementById('rminputcol'+j).value=tbtd[j-1].innerText;
    alert(tbtd[j-1].innerText);
   }
 }
}*/
