var communicateTmp=0;
/************add user dialog***********
$(document).ready(function(){
  var rgmyInv=setInterval(function(){rgshow()},0); 
    function rgshow(){        
        if($("#addusrdlg").length>0){
  $('#addusrdlg').dialog({
  autoOpen:false,
     modal:true,
     title:"Add User",
     buttons:{
                "Sure":function(){
                  
                  $(this).dialog("close");
                Shiny.onInputChange('addnewusrdlgsure',communicateTmp++);     
                },
                "Cancle":function(){
                  $(this).dialog("close");
                }         
               }
  });
   clearInterval(rgmyInv);                  //just execute once after the element exist i the dom
        }
      }
});*/

$(document).ready(function(){  
                   var readonlyprofileInv=setInterval(function(){readonlyprofileshow()},0);
    function readonlyprofileshow(){
        if($("#chgrpDlgUser").length>0){
                 $('#chgrpUsrId').attr("readonly","readonly");
                 $('#chpwdUsrId').attr("readonly","readonly");
                 $('#chgrpDlgUser').attr("readonly","readonly");
                 $('#chpwdDlgUser').attr("readonly","readonly");
                 $('#rmuser').attr("readonly","readonly");
                  clearInterval(readonlyprofileInv); 
}
}
                }); 
/********************* user profile dialog**********************/
$(document).ready(function(){
  var profileInv=setInterval(function(){profileshow()},0); 
    function profileshow(){        
        if($("#userprofiledlg").length>0){
  $('#userprofiledlg').dialog({
  autoOpen:false,
     modal:true,
     width:830,
     height:850,
     title:"User Profile",
     buttons:{
                "OK":function(){               
                  $(this).dialog("close");  
                }
               }
    
  });
   clearInterval(profileInv);                  //just execute once after the element exist i the dom
   $("#userprofiledlg").siblings("div.ui-widget-header").addClass("userprofile_header");
   $("#userprofiledlg").parent("div.ui-dialog").addClass("userprofile_body");  
   $("#userprofiledlg").siblings("div.ui-dialog-buttonpane").addClass("userprofile_footer"); 
   $("#userprofiledlg").siblings("div.ui-dialog-buttonpane").find(".ui-button").addClass("userprofile_button"); 
        }
  
    }
      
});

/********************** user log dialog**********************/
$(document).ready(function(){
  var profileInv=setInterval(function(){profileshow()},0); 
    function profileshow(){        
        if($("#usrlogdlg").length>0){
  $('#usrlogdlg').dialog({
  autoOpen:false,
     modal:true,
     width:830,
     height:860,
     title:"User activity log",
     buttons:{
                "OK":function(){           
                  $(this).dialog("close");  
                }
               },
    close:function(event,ui){
      Shiny.onInputChange('showlogdlgsure',communicateTmp++);  
    }
  });
   clearInterval(profileInv);                  //just execute once after the element exist i the dom
   $("#usrlogdlg").siblings("div.ui-widget-header").addClass("userprofile_header");
   $("#usrlogdlg").parent("div.ui-dialog").addClass("userprofile_body");  
   $("#usrlogdlg").siblings("div.ui-dialog-buttonpane").addClass("userprofile_footer"); 
   $("#usrlogdlg").siblings("div.ui-dialog-buttonpane").find(".ui-button").addClass("userprofile_button"); 
     
        }
  
    }
      
});


/*****************delete usr*********************/
$(document).ready(function(){
  var dlUsrInv=setInterval(function(){deleteUsrshow()},0); 
    function deleteUsrshow(){        
        if($("#deleteusrdlg").length>0){
  $('#deleteusrdlg').dialog({
  autoOpen:false,
     modal:true,
     width:350,
     title:"Delete User",
     buttons:{
                "Confirm":function(){
                  
                  $(this).dialog("close");
                Shiny.onInputChange('removeusrdlgsure',communicateTmp++);     
                },
                "Cancel":function(){
                  $(this).dialog("close");
                }         
               }
  });
   clearInterval(dlUsrInv);                  //just execute once after the element exist i the dom
        }
      }
});

/*****************change group*********************/
$(document).ready(function(){
  var chgrpInv=setInterval(function(){chgrpshow()},0); 
    function chgrpshow(){        
        if($("#changegrpdlg").length>0){
  $('#changegrpdlg').dialog({
  autoOpen:false,
     modal:true,
     title:"Change Group",
     height:350,
     width:350,
     buttons:{
                "Confirm":function(){
                  
                  $(this).dialog("close");
                Shiny.onInputChange('chgrpdlgsure',communicateTmp++);     
                },
                "Cancel":function(){
                  $(this).dialog("close");
                }         
               }
  });
   clearInterval(chgrpInv);                  //just execute once after the element exist i the dom
   $("#chgrpGrpId").css({"z-index":300});
        }
      }

});


/*****************change passwd*********************/
$(document).ready(function(){
  var chpwdInv=setInterval(function(){chpwdshow()},0); 
    function chpwdshow(){        
        if($("#changepwddlg").length>0){
  $('#changepwddlg').dialog({
  autoOpen:false,
     modal:true,
     title:"Change Password",
     height:330,
     width:330,
     buttons:{
                "Confirm":function(){
                  
                  $(this).dialog("close");
                Shiny.onInputChange('chpwddlgsure',communicateTmp++);     
                },
                "Cancel":function(){
                  $(this).dialog("close");
                }         
               }
  });
   clearInterval(chpwdInv);                  //just execute once after the element exist i the dom
        }
      }
      
 
     var userpasswdtypeInv=setInterval(function(){userpasswdtypeinit()},0); 
         function userpasswdtypeinit(){        
            if($("#chpwdDlgpwd").length>0&&$("#chpwdpass").length>0){
                $('#chpwdDlgpwd').attr('type','password');
                $('#chpwdpass').attr('type','password');
                clearInterval(userpasswdtypeInv);                 
                }
          }


});

/**************************************/
function generatemenu(){
  /*try{
if($('#usertable').find('table').length>0){
 $('#usertable').find('table').contextPopup({
          title: 'Control Menu',
          items: [
   
            {label:'User Profile',   icon:'icons/profile.png',       action:function() { 
              // Shiny.onInputChange('showuserpfdlg',communicateTmp++); 
              $('#userprofiledlg').dialog("open"); } },
            {label:'Remove User',   icon:'icons/remove11.png',    action:function() { $('#deleteusrdlg').dialog("open") } },
            null, // divider
            {label:'Change Group',  icon:'icons/cogs3.png',       action:function() { $('#changegrpdlg').dialog("open")} },
            {label:'Change Passwd', icon:'icons/font2.png',       action:function() { $('#changepwddlg').dialog("open") } }
           
          ]
        }); 
}}catch(e){
  alert(e.message);
}*/

$('#usertable').find('table').find("tr").contextmenu({

autoTrigger:true,
menu:[
      {title:"User Profile",uiIcon:"ui-icon-person",action:function(event,ui) { 
         Shiny.onInputChange('showuserprofile',communicateTmp++); 
         //$('#userprofiledlg').dialog("open");
        }},
        {title:"User Activity Log",uiIcon:"ui-icon-document-b",action:function(event,ui) { 
         // Shiny.onInputChange('showuserlog',communicateTmp++); 
            $('#usrlogdlg').trigger("shown");
            $('#usrlogdlg').dialog("open")
        }},
        {title:"Remove User",uiIcon:"ui-icon-closethick",action:function(event,ui) { 
        $('#deleteusrdlg').dialog("open");
        }},
        {title:"----"},
        {title:"Change Group",uiIcon:"ui-icon-wrench",action:function(event,ui) { 
         $('#changegrpdlg').dialog("open");
        }},
        {title:"Change Password",uiIcon:"ui-icon-key",action:function(event,ui) { 
         $('#changepwddlg').dialog("open");
        }}
      ]
});
}



$(document).ready(function(){
                    //$('#Addinputcol2').attr('type','password');
                    //$('#chpwdpass').attr('type','password');
                    //$('#addusrDlgpwd').attr('type','password');
                    //$('#').attr('type','password');
                   
////////////////////////////////////////////////////


////////////////////////////////////////////////////
Shiny.addCustomMessageHandler("userInforAlert",function(message){
   alert(message);
});

Shiny.addCustomMessageHandler("userpfSingleuserInfo",function(message){
  try{
   userInfo=JSON.parse(message);
   address=String(userInfo["Address"]);
   var arr=address.split("___");

   userInfo=JSON.parse(message);
   $("#userpfwitnessNa").text(userInfo["Login ID"]);
   $("#userpffirstNa").text(userInfo["First Name"]);
   $("#userpflastNa").text(userInfo["Last Name"]);
   $("#userpfGender").text(userInfo["Gender"]);
   $("#userpfemail").text(userInfo["Email"]);
   $("#userpfphonenum").text(userInfo["Phone"]);
   $("#userpfaddress1").text(arr[0]);
   $("#userpfaddress2").text(arr[1]);
   $("#userpfcity").text(arr[2]);
   $("#userfpstate").text(arr[3]);
   $("#userpfcountry").text(arr[4]);
   
   $('#userprofiledlg').dialog("open");
   }catch(err){
     alert(err.message);
   }
});

Shiny.addCustomMessageHandler("changepwdencryp",function(message){
   if(message=="Main"){
     Shiny.onInputChange('encryptedpasswdstart',communicateTmp++); 
     Shiny.onInputChange('encryptedpasswd',md5($("#chpwdpass").val().trim()+$("#chpwdUsrId").val().trim())); 
   }else{
     Shiny.onInputChange('encrypteddlgpasswdstart',communicateTmp++); 
     Shiny.onInputChange('encrypteddlgpasswd',md5($("#chpwdDlgpwd").val().trim()+$("#chpwdDlgUser").val().trim()));
   }
});

Shiny.addCustomMessageHandler("deleteUserConfirmMsg",function(message){
   $("#deleteConfirmSentence").text(message);
});

Shiny.addCustomMessageHandler("userlogSingle",function(message){
  
  /*if(message.trim()==""){
    alert("sorry, the user's log can not be found!");
    return;
  }*/
   $("#logfile").html(message);
});

});

 Shiny.onInputChange('userlogdate'," ");

/*$("body").on('click mousedown mouseup focus keydown change blur', function(e) {
     console.log(e);
});*/
