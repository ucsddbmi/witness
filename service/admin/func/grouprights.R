################################################
# group access control
################################################
reactiongrouptable<<-reactiveValues()
reactiongrouptable$a<<-0

####group table
output$grouptable<-renderDataTable({
 # input$Deletesure
 
  input$RmTheGrp_dialogConfirmButton
  reactiongrouptable$a
  #should take a 
  if(ncol(groupFeature)==0)return(data.frame(Group=""))  
  na<-names(groupFeature)
  return(data.frame(Group=na))
},
#options=list(bScrollInfinite='true',sScrollY='200px',bDestroy='true')
options=list(sScrollY=200,bDestroy=T)
,callback = "function(oTable) {
$(document).ready(function(){
  Shiny.onInputChange('grouprow','');
$('#grouptable').find('.dataTables_scrollBody').css('height',''+Math.floor($(window).height()*0.55));           //if remove the option 'sScrollY','.dataTables_scrollBody' will disappear,this sentence will be invalid
//$('#freatureTreeWrapdiv').css('height',''+$(window).height()*0.73); 
$('#freatureTreeWrapdiv').css('height',$('#grouptable').css('height'));
$('#grouptable').find('table').delegate('tr','mousedown',function(){
  if($(this).hasClass('odd')||$(this).hasClass('even')){
     if($(this).find('td').hasClass('rowclickback'))return;
    else{
         $('#grouptable').find('table').find('td.rowclickback').removeClass('rowclickback');
         $('#grouptable').find('table').find('tr.rowclickback').removeClass('rowclickback');   //we need not only do this on td but also tr 
                                                                                                //to avoid a background attributes overide
         $(this).find('td').addClass('rowclickback');
         $(this).addClass('rowclickback');
     }
     Shiny.onInputChange('grouprow',$(this).text());
     }
});
   $('#grouptable').find('table').delegate('thead','click',function(){
     Shiny.onInputChange('grouprow','');
   });
   $('#grouptable').find('label').eq(0).hide();
   $('#grouptable').find('label').eq(1).hide();

    $('#grouptable').on( 'draw.dt', function () {
   if($('#grouptable').find('table').find('td').length>0)generateGrpmenu();
   } );
})
}"
)



###remove group button
observe({   
  if(is.null(input$rmgroupconfirm))return()
  isolate({
    if(is.null(input$grouprow))return()
    if(input$grouprow=="")
    {
      session$sendCustomMessage(type="Alert","Please selected a group!") 
      return() 
    }
    #add remove check sentences at here to find if have any member in the group 
    session$sendCustomMessage(type="Confirm",paste0(" ")) 
  })
})

observe({   
  if(is.null(input$grouprow))return()
  isolate({
    #add remove check sentences at here to find if have any member in the group 
    session$sendCustomMessage(type="deleteConfirmMsg",paste0("Please confirm to delete the group ",input$grouprow," .")) 
  })
})

####remove group dialog button
observe({
  if(is.null(input$Deletesure))return()
  isolate({
    if(is.null(input$grouprow)||input$grouprow=="")return()
    if(ncol(groupFeature)<1)return()
    locs<-which(names(groupFeature)==str_trim(input$grouprow))
    if(length(locs)==0)return()
    if(length(which(str_trim(input$grouprow)==str_trim(users$Group)))>0)
    {
      session$sendCustomMessage(type="Alert","all users should be removed from the group before deleting!"); 
      return()
    }
    
  #  if(ncol(groupFeature)==2)
  #  { na<-names(groupFeature)
  #    tmpgrp<-groupFeature[,-locs]
  #    na<-na[-locs]
  #    tmpgrp<-data.frame(tmpgrp)
  #    names(tmpgrp)<-na
  #  }else{
      tmpgrp<-groupFeature[-locs]
  #  }
    assign("groupFeature",tmpgrp,envir=.GlobalEnv)

    save(tmpgrp,file=groupFeaturelink)
    
    ##########################
    #groupFeature retransform to js
    session$sendCustomMessage(type="featureAddorRmUpdate",toJSON(tmpgrp));
    
    ##########################
    #can be solved by add input$grouprow to the top of renderDataTable function of grpDtsetsTotal
    row<-which(grpDtsetsTotal[,1]==str_trim(input$grouprow))
    if(length(row)==0)return()  
    selectedDt<-grpDtsetsTotal[which(grpDtsetsTotal[row,]=="Y")]
    if(ncol(selectedDt)>0)
    {
      selectedDtNa<-names(selectedDt)
      var=1
      while(var<=length(selectedDtNa))
      {
        ctrlFilepath<-paste0(DatasetGrpAccesslink,sub(".rda","_groupCtrl.rda",str_trim(selectedDtNa[var])))
        if(file.access(ctrlFilepath)==0)
        {   
            ctrlFile=eval(parse(text=load(ctrlFilepath)))
            if(length(which(str_trim(ctrlFile[,1])==str_trim(input$grouprow)))>0){
              ctrlFile=ctrlFile[-which(str_trim(ctrlFile[,1])==str_trim(input$grouprow)),]
              save(ctrlFile,file=ctrlFilepath) 
            }
        }
        var=var+1
      }
    }
    tmp<-grpDtsetsTotal[-row,]
   # print(tmp)
    assign("grpDtsetsTotal",tmp,envir=.GlobalEnv)
    save(tmp,file=grpsDtsetsTotlink)  
   ##########################
   session$sendCustomMessage(type="rmGrp_dialogConfirmButton",str_trim(input$grouprow))
   reactiongrouptable$a<<-reactiongrouptable$a+1;
  })
})

###add group button
observe({
  if(is.null(input$addgroupconfirm)||input$addgroupconfirm<1)return()
  isolate({
    session$sendCustomMessage(type="addgrpopen"," ") 
  })
})

###add group dialog button
observe({
  if(is.null(input$addgrpsure)||input$addgrpsure=="")return()

  isolate({
    if(ncol(groupFeature)==0){
      tmp<-data.frame(rep("N",nrow(featureTree)))
      names(tmp)<-str_trim(input$addgrp)
      levels(tmp[,1])<-c("N","Y")
     # tmp<-cbind(groupFeature,tmp)
      assign("groupFeature",tmp,envir=.GlobalEnv)
      session$sendCustomMessage(type="addgrpclose"," ")
      save(tmp,file=groupFeaturelink) 
     
      session$sendCustomMessage(type="featureAddorRmUpdate",toJSON(tmp));
      ####################################
      if(length(which(str_trim(grpDtsetsTotal[,1])==str_trim(input$addgrp)))>0){
        print("group already exist in groupDatasets.rda")
        return()
      }
      tmp<-grpDtsetsTotal
      te<-rep("N",ncol(grpDtsetsTotal))
      te[1]<-str_trim(input$addgrp)
      dt<-data.frame(array(te,dim=c(1,length(te))),stringsAsFactors=F)
      names(dt)<-names(grpDtsetsTotal)
      tmp<-rbind(grpDtsetsTotal,dt);
     #if(nrow(grpDtsetsTotal)==0)names(dt)<-names(grpDtsetsTotal)
      assign("grpDtsetsTotal",tmp,envir=.GlobalEnv)
      save(tmp,file=grpsDtsetsTotlink)
    }else{
      na<-names(groupFeature)
      if(length(which(na==str_trim(input$addgrp)))>0||str_trim(input$addgrp)==""){
        if(str_trim(input$addgrp)=="") session$sendCustomMessage(type="addgrpopen","please input group name!")
        else session$sendCustomMessage(type="addgrpopen","group name already exists,please again!")
      }else{
        tmp<-data.frame(rep("N",nrow(featureTree)))
        levels(tmp[,1])<-c("N","Y")
        names(tmp)<-str_trim(input$addgrp)
        tmp<-cbind(tmp,groupFeature)
        assign("groupFeature",tmp,envir=.GlobalEnv)
        session$sendCustomMessage(type="addgrpclose"," ")
        save(tmp,file=groupFeaturelink)
        session$sendCustomMessage(type="featureAddorRmUpdate",toJSON(tmp));
        #############################
        if(length(which(str_trim(grpDtsetsTotal[,1])==str_trim(input$addgrp)))>0){
          print("group already exists!")
          return()
        }
        tmp<-grpDtsetsTotal
        te<-rep("N",ncol(grpDtsetsTotal))
        te[1]<-str_trim(input$addgrp)
        dt<-data.frame(array(te,dim=c(1,length(te))),stringsAsFactors=F)
        names(dt)<-names(grpDtsetsTotal)
        tmp<-rbind(grpDtsetsTotal,dt);
       # print(tmp)
       # if(nrow(grpDtsetsTotal)==0)names(dt)<-names(grpDtsetsTotal)
        assign("grpDtsetsTotal",tmp,envir=.GlobalEnv)
        save(tmp,file=grpsDtsetsTotlink)
       }
      }
    reactiongrouptable$a<<-reactiongrouptable$a+1;
    session$sendCustomMessage(type="UpdateGrpDtTotalAfterAdd"," ")
  })
})

output$featureDescription<-renderText({
  #remove the '\n' notation in the orginal description stenences align left and right, 
  if(is.null(input$ft_showDescription))return("Feature Description")
  if(featureTree[input$ft_showDescription,]$Description=="") return("Description: \nNo description for the feature so far!")
  else   paste0("Description:\n",featureTree[input$ft_showDescription,]$Description)
})

########send selected group for Tree
observe({
  if(is.null(input$grouprow))return()
  isolate({
    if(input$grouprow=="")session$sendCustomMessage(type="selectedGrp"," ") 
    else session$sendCustomMessage(type="selectedGrp",input$grouprow) 
  })
})

########receive Tree updated node from js
observe({
  if(is.null(input$treeCheckRecord))return()
  isolate({
    if(is.null(input$grouprow)||input$grouprow=="")return();
    notations<-fromJSON(input$treeCheckRecord)
    col<-which(input$grouprow==names(groupFeature))
    if(length(col)==0){
      print("grouprow doesn't match groupFeature col names!")
      return()
    }
    tmp=groupFeature
    tmp[,col]<- notations$record
    assign("groupFeature",tmp,envir=.GlobalEnv)
    save(tmp,file=groupFeaturelink)
  })  
})

###upload files
uploadTempDir1<<-""
uploadTempDir2<<-""

observe({
  if(is.null(input$uploadsure))return(NULL)
  isolate({
    if(isUploadValidateSuccess==F)return(NULL)
    session$sendCustomMessage(type="uploadprocessmsg","extracting and reading files......")    
    currentrow<-featureTree[as.numeric(input$ft_showDescription),]
    if(isUploadFolder==F){
      if(currentrow$currentPath=="")
      {
        path<-featureTree[currentrow$parentId,]$currentPath}
      else{
        path<-currentrow$currentPath
      }    
      Rfiles<-dir(uploadTempDir2,pattern="\\.R$",recursive=T)
      file.copy(paste0(uploadTempDir2,"/",Rfiles),path,overwrite=T)
    }else{
      files<-dir(uploadTempDir2)
      Rfolder<-files[file_test("-d",paste0(uploadTempDir2,"/",files))]
      file.copy(paste0(uploadTempDir2,"/",Rfolder),currentrow$currentPath,overwrite=T,recursive=T)
    }
    unlink(uploadTempDir1,recursive=T)
    unlink(uploadTempDir2,recursive=T)
    session$sendCustomMessage(type="uploadprocessmsg","checking and redocumenting files ......")
    recursDocAndDataExtract() 
    session$sendCustomMessage(type="uploadprocessmsg","updating Feature Tree ......")
    Init_freatureTree()
    session$sendCustomMessage(type="ft_updategrpftTab",toJSON(groupFeature));
    session$sendCustomMessage(type="ft_updateftTab",toJSON(featureTree[,c(1,2,5)]));
    session$sendCustomMessage(type="redrawfeatureTree","redraw")
    session$sendCustomMessage(type="uploaddlgclosemsg","uploadFile")
    UiServerUpdateImp()
  })
})
observe({
  if(is.null(input$uploadcancel))return(NULL)
  isolate({
    unlink(uploadTempDir1,recursive=T)
    unlink(uploadTempDir2,recursive=T)
  })
})

observe({#validation
  if(is.null(input$uploadFile$name)) return(NULL)
  isolate({
    if(is.null(input$ft_showDescription)) return(NULL)
    assign("isUploadValidateSuccess",F,envir=.GlobalEnv)
    if(length(grep("(.*?)\\.(R$|tar$|zip)$",str_trim(input$uploadFile$name)))<=0){
      session$sendCustomMessage(type="uploadvalidatemsg","Please upload files with extension: .R, .tar or .zip !")    
      return(NULL)
    }
    session$sendCustomMessage(type="uploadvalidatemsg"," ")
    tmppath<-input$uploadFile$datapath
    currentrow<-featureTree[as.numeric(input$ft_showDescription),]
    assign("isUploadFolder",F,envir=.GlobalEnv)
    if(as.numeric(input$ft_showDescription)==1||currentrow$currentPath!=""&&featureTree[which(featureTree$parentId==as.numeric(input$ft_showDescription)),][1,]$currentPath!=""){
      assign("isUploadFolder",T,envir=.GlobalEnv)
    } 
    unlink(uploadTempDir1,recursive=T)
    unlink(uploadTempDir2,recursive=T)
    dirsurfix=rnorm(200000)[1];
    assign("uploadTempDir1",paste0("/tmp/uploadtemp1",dirsurfix),envir=.GlobalEnv)
    assign("uploadTempDir2",paste0("/tmp/uploadtemp1",dirsurfix+1),envir=.GlobalEnv)
    dir.create(uploadTempDir1)
    dir.create(uploadTempDir2)
    Rfiles<-""
    if(currentrow$currentPath==""||isUploadFolder==F){
      parentpath<-featureTree[currentrow$parentId,]$currentPath
      if(length(grep("(.*?)\\.zip$",input$uploadFile$name))>0){
        file.copy(input$uploadFile$datapath,paste0(uploadTempDir1,"/",input$uploadFile$name))
        unzip(paste0(uploadTempDir1,"/",input$uploadFile$name),exdir=uploadTempDir2)
        Rfiles<-dir(uploadTempDir2,pattern="\\.R$",recursive=T)
        if(length(Rfiles)==0){
          session$sendCustomMessage(type="uploadvalidatemsg","Only .R file can be accepted in this directory!")
          unlink(uploadTempDir1,recursive=T)
          unlink(uploadTempDir2,recursive=T)
          return(NULL)
        }
      }else if(length(grep("(.*?)\\.tar$",input$uploadFile$name))>0){
        file.copy(input$uploadFile$datapath,paste0(uploadTempDir1,"/",input$uploadFile$name))
        untar(paste0(uploadTempDir1,"/",input$uploadFile$name),exdir=uploadTempDir2)
        Rfiles<-dir(uploadTempDir2,pattern="\\.R$",recursive=T)
        if(length(Rfiles)==0){
          session$sendCustomMessage(type="uploadvalidatemsg","Only .R file can be accepted in this directory!")
          unlink(uploadTempDir1,recursive=T)
          unlink(uploadTempDir2,recursive=T)
          return(NULL)
        }
      }else{
        file.copy(input$uploadFile$datapath,paste0(uploadTempDir2,"/",input$uploadFile$name))
        Rfiles=input$uploadFile$name
      }
      if(currentrow$currentPath=="")
      {
        path<-featureTree[currentrow$parentId,]$currentPath}
      else{
        path<-currentrow$currentPath
      }
      if(T%in%file.exists(paste0(path,"/",Rfiles))){
        session$sendCustomMessage(type="uploadvalidatemsg","one or more files already exist, they will be overwritten!")
      }
        
    }else {
        Rfolder<-""
      if(length(grep("(.*?)\\.zip$",input$uploadFile$name))>0){
        file.copy(input$uploadFile$datapath,paste0(uploadTempDir1,"/",input$uploadFile$name))
        unzip(paste0(uploadTempDir1,"/",input$uploadFile$name),exdir=uploadTempDir2)
        files<-dir(uploadTempDir2)
        Rfolder<-files[file_test("-d",paste0(uploadTempDir2,"/",files))]
        if(length(Rfolder)==0){
          session$sendCustomMessage(type="uploadvalidatemsg","Only folder can be accepted in this directory!")
          unlink(uploadTempDir1,recursive=T)
          unlink(uploadTempDir2,recursive=T)
          return(NULL)
        }
      }else if(length(grep("(.*?)\\.tar$",input$uploadFile$name))>0){
        file.copy(input$uploadFile$datapath,paste0(uploadTempDir1,"/",input$uploadFile$name))
        untar(paste0(uploadTempDir1,"/",input$uploadFile$name),exdir=uploadTempDir2)
        files<-dir(uploadTempDir2)
        Rfolder<-files[file_test("-d",paste0(uploadTempDir2,"/",files))]
        if(length(Rfolder)==0){
          session$sendCustomMessage(type="uploadvalidatemsg","Only folder can be accepted in this directory!")
          unlink(uploadTempDir1,recursive=T)
          unlink(uploadTempDir2,recursive=T)
          return(NULL)
        }
      }else{
        session$sendCustomMessage(type="uploadvalidatemsg","Only folder can be accepted in this directory!")
        unlink(uploadTempDir1,recursive=T)
        unlink(uploadTempDir2,recursive=T)
        return(NULL)
      }
      
      if(T%in%file.exists(paste0(currentrow$currentPath,"/",Rfolder))){
        session$sendCustomMessage(type="uploadvalidatemsg","folder already exists, it will be overwritten!")
      }
    }
    assign("isUploadValidateSuccess",T,envir=.GlobalEnv)

  })
})


#####remove source code validation
observe({
  if(is.null(input$removemenustart))return()
  isolate({
    if(is.null(input$ft_showDescription)||str_trim(input$ft_showDescription)=="")return()
    if(str_trim(featureTree[as.numeric(input$ft_showDescription),4])=="")return()
    session$sendCustomMessage(type="removesourcemsg",paste0("Coressponding source code of feature \"",featureTree[as.numeric(input$ft_showDescription),2]," \"will be removed!"))
  })
})

####remove source code
observe({
   if(is.null(input$removesourcesure))return()
   isolate({
     session$sendCustomMessage(type="removesourceprocessmsg","removing files......")
     folderintrash<-paste0(trashpath,"/",str_trim(featureTree[as.numeric(input$ft_showDescription),2]),"_",Sys.time())
     dir.create(folderintrash,mode="777")
     copyout<-file.copy(featureTree[as.numeric(input$ft_showDescription),4],folderintrash,overwrite=T,recursive=T,copy.mode=F)
     if(!copyout){print("copy directory error!")}
     unlink(featureTree[as.numeric(input$ft_showDescription),4],recursive=T,force=T)
     session$sendCustomMessage(type="removesourceprocessmsg","checking and redocumenting ......")
     recursDocAndDataExtract() 
     session$sendCustomMessage(type="removesourceprocessmsg","updating Feature Tree......")
     Init_freatureTree()
     session$sendCustomMessage(type="ft_updategrpftTab",toJSON(groupFeature));
     session$sendCustomMessage(type="ft_updateftTab",toJSON(featureTree[,c(1,2,5)]));
     session$sendCustomMessage(type="redrawfeatureTree","redraw")
     session$sendCustomMessage(type="removesourcedlgclosemsg","close")
     UiServerUpdateImp()
   })
})

tablegenerater<-function(id,caption){
 return (tags$div(style="
    border:1px solid grey;
    box-shadow:5px 5px 5px #888888;
    padding:1cm;",
           tags$div(style="text-align:center",tags$strong(caption)),              
           dataTableOutput(id)
  ))
}


output$FeatureCrl<-renderUI({
fluidPage(  
  fluidRow(
   # column(4,tablegenerater("grouptable","GroupsTable")),
   column(4,tags$div(id="grouptableWrapdiv",style="
    border:1px solid grey;
    box-shadow:5px 5px 5px #888888;
    padding:1cm;",
                     tags$div(style="text-align:center",tags$strong("Groups List")),              
                     dataTableOutput("grouptable")
   )),
   column(4,  tags$div(id="freatureTreeWrapdiv",style="
    border:1px solid grey;
    box-shadow:5px 5px 5px #888888;
    overflow:auto;
    padding:1cm;",
                       tags$div(style="text-align:center;margin-bottom:40px",tags$strong("Feature List")), 
                       tags$ul(id="featureTree",class="ztree",style="overflow:auto;vertical-align:middle;border:2px solid grey;")
                       
   )),
   column(4,tags$div(style="padding:90px 40px 20px 20px; font-style: Monospace; font-size: 16px;",verbatimTextOutput("featureDescription")))
  ),
  HTML("<br><p><strong>Groups List Modification</strong></p>"),
  fluidRow(tabsetPanel(
    tabPanel("Modification",fluidPage(actionButton("addgroupconfirm","Add Group"),actionButton("rmgroupconfirm","Remove Group"))) 
  )),
  ####################################### limit modify dialogpage ############d#####################################
  #####mobile dialog
  # HTML("<div data-role='page' data-dialog='true' id='limitdialog'>
  #     <div data-role='header'><h1>limit</h1></div>
  #     <div data-role='main' class='ui-content'><h1>lim</h1></div>
  #   
  #   </div>"),
  ######plug-in dialog
  #HTML("<div id='limitdfdlg'>
  #        <div>
  #          <table>
  #             <tr><td><label left=10px><strong>Group:&nbsp </strong></label></td><td><label id='limgrpname'>grp</label></td></tr>
  #             <tr><td><label><strong>Feature:&nbsp</strong></label></td><td><label id='limftr'>ftr</label></td></tr>
  #             <tr><td><label><strong>Type:&nbsp</strong></label></td><td><label id='limtype'>ftr</label></td></tr>
  #             <tr><td><label><strong>Limits:&nbsp </strong></label></td><td><input id='limmdf' type='text'></input></td></tr>
  #          </table>
  #       </div>
  #      </div>
  #  "),
  
  
tags$div(id='uploaddlg',
        # HTML("<img id='uploadsourceprocessimg' src='icons/loading_white.gif' class='processingimg' /><label id='showUploadwarn' style='color:red;'></label>"),
         splitLayout(cellWidths=c("85%","15%"),tags$div(id='showUploadwarn',style='text-align:justify;overflow:visible;color:red;white-space:pre-wrap;'),tags$img(id='uploadsourceprocessimg',src='icons/loading_white.gif',class='processingimg' )),
         fileInput("uploadFile","select files:",accept=c("application/tar","text/R","application/zip",".zip",".tar",".R"))
       ),

tags$div(id='removesourceloaddlg',style="margin-right:0px;",
         splitLayout(cellWidths=c("85%","15%"),tags$div(id='removesourcewarn',style='text-align:justify;overflow:visible;margin-left:20px;white-space:pre-wrap;vertical-align:middle;'),tags$img(id='removesourceprocessimg',src='icons/loading_white.gif',class='processingimg' ))
),
tags$div(id='ftTrUnselectAlert',tags$label(id="ftTrUnselectAlertString")),

  HTML("<div id='addgrpdlg'>
         <label>Please input group name:</label>
         <input id='addgrp' type='text'></input>
         <label id='adderror' style='color:red;font-size:11px'></label>
         </div>"),
  
  HTML("<div id='rmgrpconfirmdlg'>
       <table><tr>
             <td><span class=' font-size=40px width=40px style='background-color:yellow'></span></td>
             <td> <label id='confms'></label></td></tr></table>
       </div>"),
  
  HTML("<div id='alertdlg'>
       <table><tr>
             <td><span class='ui-icon ui-icon-alert' ></span></td>
       <td> <label id='alertms'></label></td></tr></table>
       </div>"),
  ######################################## load script&css #################d########################################
  tagList(
    
   # singleton(tags$head(tags$link(rel="stylesheet",type="text/css",href="grpstyle.css"))), #must use link tag import css files
    singleton(tags$head(tags$script(type="text/javascript",src="groupcontrol.js"))),
    
    singleton(tags$head(tags$script(asJSVars(ft_treeData=featureTree[,c(1,2,5)])))),
    singleton(tags$head(tags$script(asJSVars(ft_grpFtRecord=groupFeature)))),
    #print(asJSVars(ft_treeData=featureTree[,c(1,2,5)])),
    singleton(tags$head(tags$link(rel="stylesheet",href="jq_ui/jquery-ui.min.css"))),
    #singleton(tags$head(tags$script(src="jq_ui/external/jquery/jquery.js"))),
    singleton(tags$head(tags$script(src="jq_ui/jquery-ui.min.js"))),
    
    ###zTree 
    singleton(tags$head(tags$link(rel="stylesheet",href="zTree_v3/css/zTreeStyle/zTreeStyle.css"))),
    singleton(tags$head(tags$script(src="zTree_v3/js/jquery.ztree.core-3.5.js"))),
    singleton(tags$head(tags$script(src="zTree_v3/js/jquery.ztree.exedit-3.5.js"))),
    singleton(tags$head(tags$script(src="zTree_v3/js/jquery.ztree.excheck-3.5.js"))),    
    
    singleton(tags$head(tags$link(rel="stylesheet",href="context-menu/jquery.contextmenu.css"))),
    singleton(tags$head(tags$script(src="context-menu/jquery.contextmenu.js")))
    
))})

