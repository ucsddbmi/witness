$(document).ready(function(){

	/* Global Variables */
	//in participants.js, data is got from url, however, I want to get data from session.
	getTaskInfo();
	checkReadyStart();
	/* Mouse Click Listeners */
	$('button#submit').click(function(event){
		//--------added by lph 12/26---------------//
		if ( comfirmProperty())
		{
//			alert(comfirmProperty());
			var xmlhttp;
			if (window.XMLHttpRequest)
			  {// code for IE7+, Firefox, Chrome, Opera, Safari
			  xmlhttp=new XMLHttpRequest();
			  }
			else
			  {// code for IE6, IE5
			  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
			  }
			xmlhttp.onreadystatechange=function()
			  {
			  if (xmlhttp.readyState==4 && xmlhttp.status==200)
			    {				
			    }
			  }
			//document.write("checkready?'taskName'=taskName");
			var Request = GetRequest();
			var userFilePath= $("#userFilePath")[0].value;
			var userProperty = $("#userProperty")[0].value;
			xmlhttp.open("GET","./useruploadservlet?email=" + Request['email'] +"&taskName="+ Request['taskName'] + 
					"&userFilePath=" + userFilePath + "&userProperty=" + userProperty, true);
			xmlhttp.send();	
		}
		});
		//----------------------------------//

	$('button#show_submit').click(function(event){
		// show the submit data plane
//		alert("show submit");
		if($('.submitData').hasClass('hide')){
			$('.submitData').slideDown('fast');
			$('.submitData').removeClass('hide');
			$('#show_submit').html('Hide submission');
		}else{
			$('.submitData').addClass('hide');
			$('.submitData').hide();
			$('#show_submit').html('Check local data');
		}
	});

	$('button#begin_computation').click(function(event){
		/*check status of all users*/
		/*hide previous step*/
		/*start computation*/
		var dataObj = {};
		dataObj['taskName'] = $('#taskName')[0].innerHTML;
		dataObj['email'] = $('#initiatorEmail')[0].innerHTML;

		var xmlhttp;
		if (window.XMLHttpRequest)
		  {// code for IE7+, Firefox, Chrome, Opera, Safari
		  xmlhttp=new XMLHttpRequest();
		  }
		else
		  {// code for IE6, IE5
		  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
		  }
		xmlhttp.onreadystatechange=function()
		  {
		  if (xmlhttp.readyState==4 && xmlhttp.status==200)
		    {
			var xmlDoc = xmlhttp.responseXML;
			var checkresult=xmlDoc.getElementsByTagName("Checkresult")[0].childNodes[0].nodeValue;
		    if(checkresult==1) 
		    {
		    	window.location.href=xmlDoc.getElementsByTagName("Redirect")[0].childNodes[0].nodeValue;
	    	$('#createTask_title').removeClass('wrong').removeClass('check').addClass('exist');//task name exist
		    }
			
		    }
		  }
		//document.write("checkready?'taskName'=taskName");
		xmlhttp.open("GET","./beforecalservlet?email=" + $('#initiatorEmail')[0].innerHTML +"&taskName="+ $('#taskName')[0].innerHTML, true);
		xmlhttp.send();		
	});
	
});
//get information from url.
function GetRequest()

{
var url = location.search; //get information after "?"
var theRequest = new Object();
if(url.indexOf("?") != -1)
{
  var str = url.substr(1);
    strs = str.split("&");
  for(var i = 0; i < strs.length; i ++)
    {
     theRequest[strs[i].split("=")[0]]=unescape(strs[i].split("=")[1]);
    }
    return theRequest;
}else{
	return null;
}


}
//get attribute and file path from applet
function accessAppletMethod()
{
	var fn = "fn";
    fn = document.applets[0].getAttributes();
    $('#userProperty').val(fn);
//    alert(document.getElementById("taskdata_property").innerHTML);
    document.getElementById("properties").innerHTML = fn;
    fn = document.applets[0].getFilename();
    $('#userFilePath').val(fn);
    fn = document.applets[0].getAttrMeans();
    $('#userAttrMeans').val(fn);
    fn = document.applets[0].getAttrStdev();
    $('#userAttrStdev').val(fn);
    fn = document.applets[0].getMedian();
    $('#userAttrMedian').val(fn);
    fn = document.applets[0].getQtl1();
    $('#userAttrQtl1').val(fn);
    fn = document.applets[0].getQtl3();
    $('#userAttrQtl3').val(fn);
    fn = document.applets[0].getIqr1();
    $('#userAttrIqr1').val(fn);
    fn = document.applets[0].getIqr3();
    $('#userAttrIqr3').val(fn);
    fn = document.applets[0].getOut();
    $('#userAttrOut').val(fn);
    if(comfirmProperty ()){
//    	alert("pass property test");
		var ss_initiator_attribute = $('#taskproperty').val().split(/\s+/);
		var ss_initiator_attr_means = $('#initiatorAttrMeans').val().split("#");
		var ss_initiator_attr_stdev = $('#initiatorAttrStdev').val().split("#");
		var ss_initiator_attr_median = $('#initiatorAttrMedian').val().split("#");
		var ss_initiator_attr_qtl1 = $('#initiatorAttrQtl1').val().split("#");
		var ss_initiator_attr_qtl3 = $('#initiatorAttrQtl3').val().split("#");
		var ss_initiator_attr_iqr1 = $('#initiatorAttrIqr1').val().split("#");
		var ss_initiator_attr_iqr3 = $('#initiatorAttrIqr3').val().split("#");
		var ss_initiator_attr_out = $('#initiatorAttrOut').val().split("#");
		var array_means = new Array();
		var array_stdev = new Array();
		var array_median = new Array();
		var array_qtl1 = new Array();
		var array_qtl3 = new Array();
		var array_iqr1 = new Array();
		var array_iqr3 = new Array();
		var array_out = new Array();
		var ss_user_attr_means = $('#userAttrMeans').val().split("#");
		var ss_user_attr_stdev = $('#userAttrStdev').val().split("#");
		var ss_user_attr_median = $('#userAttrMedian').val().split("#");
		var ss_user_attr_qtl1 = $('#userAttrQtl1').val().split("#");
		var ss_user_attr_qtl3 = $('#userAttrQtl3').val().split("#");
		var ss_user_attr_iqr1 = $('#userAttrIqr1').val().split("#");
		var ss_user_attr_iqr3 = $('#userAttrIqr3').val().split("#");
		var ss_user_attr_out = $('#userAttrOut').val().split("#");
		
		array_means[0] = ss_initiator_attr_means;
		array_stdev[0] = ss_initiator_attr_stdev;
		array_median[0] = ss_initiator_attr_median;
		array_qtl1[0] = ss_initiator_attr_qtl1;
		array_qtl3[0] = ss_initiator_attr_qtl3;
		array_iqr1[0] = ss_initiator_attr_iqr1;
		array_iqr3[0] = ss_initiator_attr_iqr3;
		

		
		array_means[1] = ss_user_attr_means;
		array_stdev[1] = ss_user_attr_stdev;
		array_median[1] = ss_user_attr_median;
		array_qtl1[1] = ss_user_attr_qtl1;
		array_qtl3[1] = ss_user_attr_qtl3;
		array_iqr1[1] = ss_user_attr_iqr1;
		array_iqr3[1] = ss_user_attr_iqr3;
		array_out[0] = new Array();
		for( var i=0; i < ss_initiator_attr_out.length; i++){
			array_out[0][i] = ss_initiator_attr_out[i].split("@");
		}
		array_out[1] = new Array();
		for( var i=0; i < ss_user_attr_out.length; i++){
			array_out[1][i] = ss_user_attr_out[i].split("@");
		}
		
//		plotHistGram(['Initiator', 'Participant'], ss_initiator_attribute, array_median, array_qtl1, array_qtl3, array_iqr1, array_iqr3, array_out);
		clearCanvas("canvas1");
		boxPlot(['Initiator', 'Participant'], ss_initiator_attribute, array_median, array_qtl1, array_qtl3, array_iqr1, array_iqr3, array_out);
		
    }
}

function getTaskInfo(){
//	alert(GetRequest()['taskName']);
//	if(GetRequest()['taskName'] & GetRequest()['email']){
	if(GetRequest()!=null){
		var Request = GetRequest();
		var taskName=Request['taskName'];
		var partEmail = Request['email'];
		//to get initiator email
//		$('#initiatorEmail')[0].innerHTML = initEmail;
		$('#taskName')[0].innerHTML = taskName;
		$('#begin_computation').hide();
	}
//	var Request = GetRequest();
//	var task=Request['taskName'];
//	var email = Request['email'];
	else{
		$('#show_submit').hide();
		$.ajax({type:'POST', async:false, url: './getTaskInfo', 
		success: function(data){
			var statusInfo = eval("(" + data + ")");
			var taskName = statusInfo[0].taskName;
			var initEmail = statusInfo[0].initEmail;
			var err = statusInfo[0].error;
			$('#initiatorEmail')[0].innerHTML = initEmail;
			$('#taskName')[0].innerHTML = taskName;
			alert("task Name is " + taskName + " initEmail is " + initEmail);
		}
		});	
	}
}

function checkReadyContinuous(task,useremail){
	function checkReady(){
		var xmlhttp;    
		var taskName = task;
		var email=useremail;
		if (taskName==""){
		  document.getElementById("readyStatus").innerHTML="";
		  return;
		  }
		if (window.XMLHttpRequest)
		  {// code for IE7+, Firefox, Chrome, Opera, Safari
		  xmlhttp=new XMLHttpRequest();
		  }
		else
		  {// code for IE6, IE5
		  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
		  }
		xmlhttp.onreadystatechange=function()
		  {
		  if (xmlhttp.readyState==4 && xmlhttp.status==200)
		    {
		  	document.getElementById("readyStatus").innerHTML=xmlhttp.responseText;
			var xmlDoc = xmlhttp.responseXML;
		  	var table = "";
		  	table = table + "<table>";
		  	table = table + "<tr> <td> Email </td> <td> Participant Status </td> <td> TaskStatus </td> </tr>";
		  	for( var i=0; i < xmlDoc.getElementsByTagName("TaskStatus").length; i++){
		  		table = table + "<tr> <td> "+ xmlDoc.getElementsByTagName("Email")[i].childNodes[0].nodeValue 
		  		+" </td> <td> "+ xmlDoc.getElementsByTagName("Status")[i].childNodes[0].nodeValue +" </td> <td> "
		  		+ xmlDoc.getElementsByTagName("TaskStatus")[i].childNodes[0].nodeValue+" </td> </tr>";
		  	}
		  	table = table + "</table>";
		  	document.getElementById("readyStatus").innerHTML = table;
			//  check if computation start
		  	var isUserReady;
		  	for( var i=0;i< xmlDoc.getElementsByTagName("Status").length; i++)
		  	{
		  		if (email==xmlDoc.getElementsByTagName("Email")[i].childNodes[0].nodeValue)
		  		{
		  			isUserReady=xmlDoc.getElementsByTagName("Status")[i].childNodes[0].nodeValue;
		  			break;
		  		}
		  	}
			if(xmlDoc.getElementsByTagName("TaskStatus").length> 0 && xmlDoc.getElementsByTagName("Status").length> 0)
			{
				var isTaskBegin = xmlDoc.getElementsByTagName("TaskStatus")[0].childNodes[0].nodeValue;
				var isUserReady = xmlDoc.getElementsByTagName("SelfStatus")[0].childNodes[0].nodeValue;
				if(isTaskBegin == "1" &&isUserReady=="1"){//
					
					//var email = "<%=email %>";
					var url = "computation.jsp?taskName=" + taskName + "&email=" + email;
					window.location.href=url;
				}
				else if (isTaskBegin != "0" && isUserReady=="0"){						
					alert("Task has began. You missed this task!");
					var url = "home.html";
					window.location.href=url;
				}
			}
			else
			{
//				alert("XML data fortmat is wrong and the original text content is:"+xmlhttp.responseText);
			}
		    }
		  }
		//document.write("checkready?'taskName'=taskName");
		xmlhttp.open("GET","checkready?email=" + email+"&taskName="+taskName, true);
		xmlhttp.send();		
	}
//	checkReady();
	setInterval(checkReady, 2000);
}

function checkPropertyContinuous(task){
	function checkPropertyReady(){
		var taskName = task;
		$.ajax({type:'POST', url: './gettaskpropertyservlet?taskName=' + taskName, 
		success: function(data){
//			alert(data);
			var statusInfo = eval("(" + data + ")");
			var taskproperty = statusInfo[0].taskproperty;
			var ownerEmail = statusInfo[0].ownerEmail;
			var attr_means = statusInfo[0].attr_means;
			var attr_stdev = statusInfo[0].attr_stdev;
			var attr_median = statusInfo[0].attr_median;
			var attr_qtl1 = statusInfo[0].attr_qtl1;
			var attr_qtl3 = statusInfo[0].attr_qtl3;
			var attr_iqr1 = statusInfo[0].attr_iqr1;
			var attr_iqr3 = statusInfo[0].attr_iqr3;
			var attr_out = statusInfo[0].attr_out;
			var err = statusInfo[0].error;
			
			/*show the results on the page*/
			$("#taskdata_property")[0].innerHTML = taskproperty;
			$('#initiatorEmail')[0].innerHTML = ownerEmail;
			$('#taskproperty').val(taskproperty);
			$('#initiatorAttrMeans').val(attr_means);
			$('#initiatorAttrStdev').val(attr_stdev);
			$('#initiatorAttrMedian').val(attr_median);
			$('#initiatorAttrQtl1').val(attr_qtl1);
			$('#initiatorAttrQtl3').val(attr_qtl3);
			$('#initiatorAttrIqr1').val(attr_iqr1);
			$('#initiatorAttrIqr3').val(attr_iqr3);
			$('#initiatorAttrOut').val(attr_out);
			 
			/*get error information from data and show in UI*/
			$('#error_display').empty();
			$('#error_display').append('<p>' + err + '<br><br><br></p>');
			
			/*plot hist gram*/
			//split string
			var ss_attribute = taskproperty.split(/\s+/);
			var ss_attr_means = attr_means.split("#");
			var ss_attr_stdev = attr_stdev.split("#");
			var ss_attr_median = attr_median.split("#");
			var ss_attr_qtl1 = attr_qtl1.split("#");
			var ss_attr_qtl3 = attr_qtl3.split("#");
			var ss_attr_iqr1 = attr_iqr1.split("#");
			var ss_attr_iqr3 = attr_iqr3.split("#");
			var ss_attr_out = attr_out.split("#");
			
			var array_means = new Array();
			var array_stdev = new Array();
			var array_median = new Array();
			var array_qtl1 = new Array();
			var array_qtl3 = new Array();
			var array_iqr1 = new Array();
			var array_iqr3 = new Array();
			var array_out = new Array();
			array_means[0] = ss_attr_means;
			array_stdev[0] = ss_attr_stdev;
			array_median[0] = ss_attr_median;
			array_qtl1[0] = ss_attr_qtl1;
			array_qtl3[0] = ss_attr_qtl3;
			array_iqr1[0] = ss_attr_iqr1;
			array_iqr3[0] = ss_attr_iqr3;
			array_out[0] = new Array();
			for( var i=0; i < ss_attr_out.length; i++){
				array_out[0][i] = ss_attr_out[i].split("@");
			}
//			plotHistGram(['Initiator'], ss_attribute, array_means, array_stdev);
			clearCanvas("canvas1");
			boxPlot(['Initiator'], ss_attribute, array_median, array_qtl1, array_qtl3, array_iqr1, array_iqr3, array_out);
		}
	});
	}
	 checkPropertyReady();
//	setInterval(checkPropertyReady, 2000);
}

function checkReadyStart(){
	//var task = "hello8";
//	var Request = GetRequest();
//	var task=Request['taskName'];
//	var email = Request['email'];
	var task, Request, email;
	if(GetRequest() !=null){
		 Request = GetRequest();
		 task=Request['taskName'];
		 email = Request['email'];
	}
	else{
		email = $('#initiatorEmail')[0].innerHTML;
		 task = $('#taskName')[0].innerHTML;
	}
//	var email = $('#initiatorEmail')[0].innerHTML;
//	var task = $('#taskName')[0].innerHTML;
	checkReadyContinuous(task, email);
	checkPropertyContinuous(task);
}

function comfirmProperty ()
{
	var $form = $('form#joinTask');
	var $taskProperty;
	var $userProperty;
	var flag = 1;
	$form.find('input').each(function(){
		var id = this.id;
		var $value = $('#'+id).val(); 
		switch (id)
		{
			case "taskProperty": $taskProperty = $value; break;
			case "userProperty": $userProperty = $value; break;
		}
	});
	//if(checkTaskname())
	//{
	//	$('#createTask_title').removeClass('check').addClass('exist');
	//	flag = 0;
	//}
	if ( $userProperty == $taskProperty )
	{
		$('#choosefile').removeClass('wrong').addClass('check');
		flag = 1;
	}
	else
	{
		$('#choosefile').removeClass('check').addClass('wrong');
		flag=0;
	}
	return flag;
}

function plotHistGram (compare, attributes, means, stdev)
{
	var cx1 = new CanvasXpress('canvas1',
      {
   /* 'x' : {
      'Species' : ['age',  'height',  'speed']
	 },*/
        'y' : {
     /* 'vars' : ['Person1', 'Person2'],
      'smps' : ['age',  'height',  'speed'],
      'data' : [[1, 1.1, 1.2], [2, 2.5, 3]],
	  'stdev': [[0.5, 0.3, 0.4], [1, 1, 1]],*/
	  
	  'vars' : compare,
      'smps' : attributes,
      'data' : means,
	  'stdev': stdev,
      'desc' : ['Values']
    } },
      {'axisTickFontStyle': 'bold',
      'axisTickScaleFontFactor': 1.5,
      'axisTitleFontStyle': 'italic',
      'axisTitleScaleFontFactor': 1.5,
      'fontStyle': 'italic',
      'graphOrientation': 'vertical',
      'graphType': 'Bar',
      'legendBox': false,
      'legendScaleFontFactor': 1.2,
      'marginBottom': 30,
      'showShadow': true,
      'smpLabelFontStyle': 'italic',
      'smpLabelRotate': 45,
      'smpLabelScaleFontFactor': 1,
      'smpTitle': 'Data Attributes',
      'smpTitleScaleFontFactor': 1.5,
      'title': 'Check Data Set',
      'titleHeight': 60,
      'xAxis2Show': false}
    );

}

function boxPlot (compare, attributes, median, qtl1, qtl3, iqr1, iqr3, outValue){
		
		var cx1 = new CanvasXpress('canvas1',
          {
           /* 'x' : {
              'Species' : ['age',  'height',  'speed']
            },*/
            'y' : {
              'vars' : compare,
              'smps' : attributes,
			  'data': median,
			  "median": median,
			  'qtl1': qtl1,
			  'qtl3': qtl3,
			  'iqr1': iqr1,
			  'iqr3': iqr3,
			  'out': outValue,
              'desc' : ['Value']
            }
          },
          {'axisTickFontStyle': 'bold',
          'axisTickScaleFontFactor': 1.5,
          'axisTitleFontStyle': 'italic',
          'axisTitleScaleFontFactor': 1.5,
          'citationFontStyle': 'italic',
          'fontStyle': 'italic',
          'graphOrientation': 'vertical',
          'graphType': 'Boxplot',
          'legendBox': false,
          'legendScaleFontFactor': 1.2,
          'marginBottom': 30,
          'showShadow': true,
          'smpLabelFontStyle': 'italic',
          'smpLabelRotate': 45,
          'smpLabelScaleFontFactor': 0.5,
          'smpTitle': 'Data Attributes',
          'smpTitleScaleFontFactor': 1.5,
          'title': 'Check Data Set',
          'titleHeight': 60,
          'xAxis2Show': false}
        );
        //.groupSamples(["Species"]);
}

function clearCanvas(canvasName){
	var c=document.getElementById(canvasName);
	c.height = c.height;
}

