$(document).ready(function(){

	/* Global Variables */
	/* Mouse Click Listeners */
	$('#ScoreMatch').click(function(event){
		getScoreMatch();
	});
});

function getReport(task){
	var xmlhttp;    
	var taskName = task;
	if (taskName==""){
	  return;
	  }
	if (window.XMLHttpRequest)
	  {// code for IE7+, Firefox, Chrome, Opera, Safari
	  xmlhttp=new XMLHttpRequest();
	  }
	else
	  {// code for IE6, IE5
	  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
	  }
	xmlhttp.onreadystatechange=function()
	{
	  if (xmlhttp.readyState==4 && xmlhttp.status==200)
	    {
			//send taskName to the server and get the URL to redirect
			var url = xmlhttp.responseText;
		  	document.getElementById("reportURL").innerHTML= url;
		    window.location.href = url;
		}
	}
	xmlhttp.open("GET","getreportservlet?taskName=" + taskName, true);
	xmlhttp.send();		
}
function getLocalReport(task){
	var xmlhttp;    
	var taskName = task;
	if (taskName==""){
	  return;
	  }
	if (window.XMLHttpRequest)
	  {// code for IE7+, Firefox, Chrome, Opera, Safari
	  xmlhttp=new XMLHttpRequest();
	  }
	else
	  {// code for IE6, IE5
	  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
	  }
	xmlhttp.onreadystatechange=function()
	{
	  if (xmlhttp.readyState==4 && xmlhttp.status==200)
	    {
			//send taskName to the server and get the URL to redirect
			var url = xmlhttp.responseText;
		  	document.getElementById("LocalreportURL").innerHTML= url;
		    window.location.href = url;
		}
	}
	xmlhttp.open("GET","getlocalreportservlet?taskName=" + taskName, true);
	xmlhttp.send();		
}

function accessAppletMethod2()
{
	var fn = "fn";
	alert("in accessAppletMethod2");
    fn = document.applets[1].getURL();
    document.getElementById('LocalreportURL').value = fn;
}

function enableButton()
{
	var gB = document.getElementById("globalButton");	
	gB.disabled=false;
	var gB2 = document.getElementById("TestButton");	
	gB2.disabled=false;
	var gB3 = document.getElementById("ScoreMatch");
	gB3.disabled=false;
	 document.applets[1].setEnable();
}

function getScoreMatch(){
	$.ajax({type:'POST', url: './PScoreMatchingServlet', 
		success: function(data){
//				alert(data);
				//The structure of data: [min p score, matched pscore, participants, minId]
				var matchingMap = eval("(" + data + ")");;
//				var matchingMap = eval(data );
				var matchResult = new Array();
//				var participant = new Array();
				var participant = matchingMap[2];
				var minId = matchingMap[3];
//				participant[0] = 'Participant0';
//				participant[1] = 'Participant1';
//				participant[2] = 'Participant2';
				var smps = new Array();
				smps[0] = "Negative";
				smps[1] = "Positive";
				for(var i = 0; i < 2; i++){
//					matchResult[i] = new Array();
					var index = i;
					//the negative/positive match depend on minId
					if( minId == 1){
						index = 1 - i;
					}
					for(var j = 0; j < matchingMap[i].length; j++){
						if(matchResult[matchingMap[i][j]] === undefined){
							matchResult[matchingMap[i][j]] = new Array();
							matchResult[matchingMap[i][j]][index] = 1;
//							participant[matchingMap[i][j]] = 'Participant' + i;
						}
						else{
							if(matchResult[matchingMap[i][j]][index] === undefined){
								matchResult[matchingMap[i][j]][index] = 1;
							}
							else{
								matchResult[matchingMap[i][j]][index] = matchResult[matchingMap[i][j]][index] + 1;
							}
						}
					}
				}
//			plotHistDiagram(participant, matchResult, smps);
			pieDiagram(participant, matchResult, smps);
	        console.log(JSON.stringify(data));
		}
	});
}

function plotHistDiagram (vars, matchData, smps)
{
	var cx1 = new CanvasXpress('histDiagram',
      {
   /* 'x' : {
      'Species' : ['age',  'height',  'speed']
	 },*/
        'y' : {
     /* 'vars' : ['Person1', 'Person2'],
      'smps' : ['age',  'height',  'speed'],
      'data' : [[1, 1.1, 1.2], [2, 2.5, 3]],
	  'stdev': [[0.5, 0.3, 0.4], [1, 1, 1]],*/
	  
	  'vars' : vars,
      'smps' : smps,
      'data' : matchData,
      'desc' : ['Values']
    } },
      {'axisTickFontStyle': 'bold',
      'axisTickScaleFontFactor': 1.5,
      'axisTitleFontStyle': 'italic',
      'axisTitleScaleFontFactor': 1.5,
      'fontStyle': 'italic',
      'graphOrientation': 'vertical',
      'graphType': 'Bar',
      'legendBox': false,
      'legendScaleFontFactor': 1.2,
      'marginBottom': 30,
      'showShadow': true,
      'smpLabelFontStyle': 'italic',
      'smpLabelRotate': 90,
      'smpLabelScaleFontFactor': 1,
      'smpTitle': 'P Score Type',
      'smpTitleScaleFontFactor': 1.5,
      'title': 'P Score Match',
      'titleHeight': 60,
      'xAxis2Show': false}
    );

}

function pieDiagram (vars, matchData, smps)
{
        var cx1 = new CanvasXpress('pieDiagram',
          {

            'y' : {
//              'vars' : ['Variable1', 'Variable2', 'Variable3' ],
				'vars' : vars,
//				'smps' : ['Positive', 'Negative'],
              	'smps' : smps,
//              'data' : [[5, 10 ], [95, 80], [25, 30],],
				'data' : matchData,
              'desc' : ['Magnitude1', 'Magnitude2']
            }
          },
          {'graphType': 'Pie',
//          'layout': '1X2',
          'pieSegmentLabels': 'inside',
          'pieSegmentPrecision': 0,
          'pieSegmentSeparation': 1,
          'showPieGrid': false,
          'showPieSampleLabel': true,
          'title': 'P Score Match in Pie Diagram',
//          'xAxis': ['Positive', 'Negative',]
          'xAxis': smps
 			}
        );
}
