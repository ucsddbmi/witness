$(document).ready(function() { 
//	$('button#Upload').submit(function (event){
		$('#uploadTestFile').fileupload({
			url: './PScoreMatchingServlet',
			done:function(e,result){  
//				alert(result);
				var matchingMap = eval(result.result);
				var matchResult = new Array();
				var participant = new Array();
				participant[0] = 'Participant0';
				participant[1] = 'Participant1';
				participant[2] = 'Participant2';
				var smps = new Array();
				smps[0] = "Positive Match";
				smps[1] = "Negative Match";
				for(var i = 0; i < matchingMap.length; i++){
//					matchResult[i] = new Array();
					
					for(var j = 0; j < matchingMap[i].length; j++){
						if(matchResult[matchingMap[i][j]] === undefined){
							matchResult[matchingMap[i][j]] = new Array();
							matchResult[matchingMap[i][j]][i] = 1;
						}
						else{
							if(matchResult[matchingMap[i][j]][i] === undefined){
								matchResult[matchingMap[i][j]][i] = 1;
							}
							else{
								matchResult[matchingMap[i][j]][i] = matchResult[matchingMap[i][j]][i] + 1;
							}
						}
					}
				}
			pieGram2(participant, matchResult, smps);
	        console.log(JSON.stringify(result.result));  
	    }
		});
//		return false;
//	});
}); 

function plotHistGram (compare, attributes, means, stdev)
{
	var cx1 = new CanvasXpress('positiveMatch',
      {
   /* 'x' : {
      'Species' : ['age',  'height',  'speed']
	 },*/
        'y' : {
     /* 'vars' : ['Person1', 'Person2'],
      'smps' : ['age',  'height',  'speed'],
      'data' : [[1, 1.1, 1.2], [2, 2.5, 3]],
	  'stdev': [[0.5, 0.3, 0.4], [1, 1, 1]],*/
	  
	  'vars' : compare,
      'smps' : attributes,
      'data' : means,
	  'stdev': stdev,
      'desc' : ['Values']
    } },
      {'axisTickFontStyle': 'bold',
      'axisTickScaleFontFactor': 1.5,
      'axisTitleFontStyle': 'italic',
      'axisTitleScaleFontFactor': 1.5,
      'fontStyle': 'italic',
      'graphOrientation': 'vertical',
      'graphType': 'Bar',
      'legendBox': false,
      'legendScaleFontFactor': 1.2,
      'marginBottom': 30,
      'showShadow': true,
      'smpLabelFontStyle': 'italic',
      'smpLabelRotate': 45,
      'smpLabelScaleFontFactor': 1,
      'smpTitle': 'Data Attributes',
      'smpTitleScaleFontFactor': 1.5,
      'title': 'Check Data Set',
      'titleHeight': 60,
      'xAxis2Show': false}
    );

}

function pieGram (vars, data, smps)
{
	var cx1 = new CanvasXpress('positiveMatch',
    {
        'y' : {
//          'vars' : vars,
//          'smps' : smps,
//          'data' : data,
			'vars' : ['part1', 'part2', 'part3'],
			'smps' : ['positive', 'negative'],
			'data' : [[1, 2, 3], [1, 1, 1]],
          'desc' : ['Magnitude1', 'Magnitude2']
        }
//        'a' : {
//          'xAxis' : ['Variable1', 'Variable2'],
//          'xAxis2' : ['Variable3', 'Variable4']
//        },
//        't' : {
//          'vars' : '(((Variable1,Variable3),Variable4),Variable2)',
//          'smps' : '(((((Sample1,Sample2),Sample3),Sample4),Sample5),Sample6)'
//        }
      },
      {'graphType': 'Pie',
//      'layout': '1X2',
      'pieSegmentLabels': 'inside',
      'pieSegmentPrecision': 0,
      'pieSegmentSeparation': 1,
      'showPieGrid': true,
      'showPieSampleLabel': true,
//      'xAxis': vars
      }
    );
}

function pieGram2 (vars, matchData, smps)
{
        var cx1 = new CanvasXpress('positiveMatch',
          {

            'y' : {
//              'vars' : ['Variable1', 'Variable2', 'Variable3' ],
				'vars' : vars,
              'smps' : ['Sample1', 'Sample2', 'Sample3', 'Sample4', 'Sample5', 'Sample6'],
//              'data' : [[5, 10 ], [95, 80], [25, 30],],
				'data' : matchData,
              'desc' : ['Magnitude1', 'Magnitude2']
            }
          },
          {'graphType': 'Pie',
          'layout': '1X2',
          'pieSegmentLabels': 'inside',
          'pieSegmentPrecision': 0,
          'pieSegmentSeparation': 1,
          'showPieGrid': true,
          'showPieSampleLabel': true,
          'xAxis': ['Sample1', 'Sample2', ]
//          'xAxis': smps
 			}
        );
}